# Database Backup#
# Backup Date: 2016-02-11 15:51:41

drop table if exists bankmaster;
create table bankmaster (
  bankId int(6) not null auto_increment,
  bankName varchar(60) not null ,
  phone1 varchar(12) not null ,
  phone2 varchar(12) not null ,
  PRIMARY KEY (bankId)
);

insert into bankmaster (bankId, bankName, phone1, phone2) values ('1', 'Bill', '', '');
drop table if exists bhavcopy;
create table bhavcopy (
  bhavcopyid int(10) not null auto_increment,
  exchange varchar(30) not null ,
  bhavcopyDate date default '0000-00-00' not null ,
  sessionId varchar(15) not null ,
  marketType varchar(15) not null ,
  instrumentId int(10) default '0' not null ,
  instrumentName varchar(15) not null ,
  scriptCode int(10) default '0' not null ,
  contractCode varchar(20) not null ,
  scriptGroup varchar(5) not null ,
  scriptType varchar(5) not null ,
  expiryDate date default '0000-00-00' not null ,
  expiryDateBc varchar(10) not null ,
  strikePrice float default '0' not null ,
  optionType varchar(4) not null ,
  previousClosePrice float default '0' not null ,
  openPrice float default '0' not null ,
  highPrice float default '0' not null ,
  lowPrice float default '0' not null ,
  closePrice float default '0' not null ,
  totalQtyTrade int(10) default '0' not null ,
  totalValueTrade double default '0' not null ,
  lifeHigh float default '0' not null ,
  lifeLow float default '0' not null ,
  quoteUnits varchar(10) not null ,
  settlementPrice float default '0' not null ,
  noOfTrades int(6) default '0' not null ,
  openInterest double default '0' not null ,
  avgTradePrice float default '0' not null ,
  tdcl float default '0' not null ,
  lstTradePrice float default '0' not null ,
  remarks text not null ,
  PRIMARY KEY (bhavcopyid)
);

drop table if exists cashflow;
create table cashflow (
  cashFlowId int(6) not null auto_increment,
  clientId int(10) default '0' not null ,
  itemIdExpiryDate varchar(500) ,
  dwStatus char(2) not null ,
  dwAmount float default '0' not null ,
  plStatus char(2) not null ,
  plAmount float default '0' not null ,
  transactionDate date ,
  transType varchar(20) ,
  transMode varchar(30) not null ,
  notes varchar(1000) ,
  PRIMARY KEY (cashFlowId)
);

insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('3', '3', 'Comm', 'w', '47260', '', '0', '2013-05-27', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('4', '3', 'Cash at her home', 'd', '47660', '', '0', '2013-05-28', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('5', '20', 'Comm', 'w', '3080', '', '0', '2013-05-27', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('11', '3', 'Comm', 'd', '3240', '', '0', '2013-06-01', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('12', '20', 'Comm', 'd', '30520', '', '0', '2013-06-01', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('13', '20', 'Tax', 'd', '1183', '', '0', '2013-06-01', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('17', '12', 'Comm', 'd', '2930', '', '0', '2013-06-01', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('20', '20', '25000 Peter', 'w', '9000', '', '0', '2013-06-01', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('21', '3', 'Cash at her home', 'w', '3640', '', '0', '2013-06-04', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('27', '3', 'Cash at her home', 'w', '5100', '', '0', '2013-06-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('28', '3', 'Comm', 'd', '5100', '', '0', '2013-06-08', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('29', '20', 'Comm', 'd', '5600', '', '0', '2013-06-08', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('30', '20', 'Chq', 'w', '67993', '', '0', '2013-06-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('33', '12', 'Comm', 'd', '18515', '', '0', '2013-06-08', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('35', '20', 'Bhul', 'd', '200', '', '0', '2013-06-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('39', '20', 'Parin', 'd', '26000', '', '0', '2013-06-10', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('42', '3', 'Comm', 'd', '49150', '', '0', '2013-06-15', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('43', '20', 'Parin', 'w', '1000', '', '0', '2013-06-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('44', '20', 'Comm', 'w', '12105', '', '0', '2013-06-15', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('48', '12', 'Comm', 'w', '174135', '', '0', '2013-06-15', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('57', '3', 'Cash at her home', 'w', '49150', '', '0', '2013-06-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('62', '12', 'Cash at her shop', 'd', '152870', '', '0', '2013-06-18', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('66', '3', 'comm', 'd', '12320', '', '0', '2013-06-24', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('67', '3', 'cash at her home', 'w', '12320', '', '0', '2013-06-24', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('68', '20', 'Comm', 'd', '76090', '', '0', '2013-06-24', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('74', '12', 'Comm', 'd', '612215', '', '0', '2013-06-26', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('75', '12', 'Cash at my off', 'w', '612400', '', '0', '2013-06-26', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('80', '5', 'Fo', 'w', '8092', '', '0', '2013-06-26', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('88', '20', 'comm', 'd', '150390', '', '0', '2013-06-29', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('89', '3', 'comm', 'w', '110240', '', '0', '2013-06-29', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('94', '12', 'comm', 'd', '188075', '', '0', '2013-06-29', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('105', '5', 'fo', 'd', '28470', '', '0', '2013-06-29', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('108', '3', 'cash at her home', 'd', '110240', '', '0', '2013-07-03', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('109', '20', 'p.umesh', 'w', '196800', '', '0', '2013-07-06', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('110', '12', 'cash at my shop', 'w', '188000', '', '0', '2013-07-03', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('113', '3', 'comm', 'd', '20350', '', '0', '2013-07-06', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('114', '20', 'comm', 'd', '87710', '', '0', '2013-07-06', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('119', '2', 'comm', 'w', '17400', '', '0', '2013-07-06', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('121', '12', 'comm', 'w', '188415', '', '0', '2013-07-06', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('126', '12', 'Mint2', 'd', '25000', '', '0', '2013-07-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('127', '12', 'Cash at her off', 'd', '163340', '', '0', '2013-07-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('128', '3', 'Cash at her home', 'w', '20350', '', '0', '2013-07-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('129', '20', 'P.umesh', 'w', '87700', '', '0', '2013-07-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('130', '5', 'Bhul', 'd', '3322', '', '0', '2013-07-09', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('132', '3', 'Comm', 'w', '75870', '', '0', '2013-07-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('133', '3', 'Cash at her home', 'd', '75870', '', '0', '2013-07-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('134', '20', 'Comm', 'w', '138070', '', '0', '2013-07-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('135', '20', 'P.umesh', 'd', '120000', '', '0', '2013-07-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('138', '2', 'Comm', 'w', '4200', '', '0', '2013-07-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('141', '12', 'Comm', 'w', '259195', '', '0', '2013-07-15', 'Other', 'Bill', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('142', '12', 'Md', 'w', '8750', '', '0', '2013-07-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('143', '5', 'Do', 'd', '13001', '', '0', '2013-07-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('144', '5', 'P.umesh', 'w', '36701', '', '0', '2013-07-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('145', '12', 'Cash at off', 'd', '267950', '', '0', '2013-07-20', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('149', '2', 'P.sailesh', 'd', '17400', '', '0', '2013-07-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('151', '20', 'Comm', 'd', '54830', '', '0', '2013-07-20', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('155', '2', 'Comm', 'w', '12700', '', '0', '2013-07-20', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('157', '12', 'Comm', 'd', '67420', '', '0', '2013-07-20', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('158', '12', '25%', 'w', '1825', '', '0', '2013-07-20', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('163', '12', 'Dt 2 mint', 'd', '136608', '', '0', '2013-07-22', 'Other', 'ClientToClient', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('164', '12', 'Cash at my shop harsh', 'w', '202200', '', '0', '2013-07-22', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('166', '20', 'Comm', 'd', '10655', '', '0', '2013-08-03', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('169', '2', 'Comm', 'd', '4700', '', '0', '2013-08-03', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('172', '12', 'Comm', 'w', '52290', '', '0', '2013-08-03', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('173', '12', '25%', 'w', '2747', '', '0', '2013-08-03', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('178', '12', 'Cash at her off', 'd', '55000', '', '0', '2013-08-03', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('179', '2', 'P.sailesh', 'd', '16900', '', '0', '2013-07-29', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('181', '20', 'Comm', 'w', '199425', '', '0', '2013-08-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('186', '12', 'Comm', 'w', '798425', '', '0', '2013-08-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('188', '20', 'Bhav2ar', 'w', '-130000', '', '0', '2013-08-08', 'Other', 'ClientToClient', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('192', '12', 'Cash at her off', 'd', '500000', '', '0', '2013-08-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('193', '12', 'Cash at her off', 'd', '123270', '', '0', '2013-08-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('194', '12', '25%', 'd', '26730', '', '0', '2013-08-08', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('197', '3', 'Comm', 'd', '8270', '', '0', '2013-08-10', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('198', '20', 'Comm', 'd', '25620', '', '0', '2013-08-10', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('199', '20', 'Saheb', 'd', '3000', '', '0', '2013-08-10', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('204', '12', 'Comm', 'd', '67885', '', '0', '2013-08-10', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('205', '2', 'Comm', 'w', '530', '', '0', '2013-08-10', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('214', '12', 'Dt2mint', 'd', '-33005', '', '0', '2013-08-10', 'Other', 'ClientToClient', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('216', '20', 'P.umesh mob', 'w', '30000', '', '0', '2013-08-12', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('217', '3', 'Cash at her home', 'w', '8270', '', '0', '2013-08-10', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('219', '12', 'Cash at off', 'd', '113579', '', '0', '2013-08-10', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('223', '20', 'Comm', 'd', '37835', '', '0', '2013-08-17', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('224', '20', 'P.umesh', 'w', '40960', '', '0', '2013-08-17', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('229', '12', 'Comm', 'w', '3067340', '', '0', '2013-08-17', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('230', '12', 'Cash at my home', 'd', '200000', '', '0', '2013-08-17', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('234', '12', '25%', 'd', '71363', '', '0', '2013-08-17', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('235', '20', 'Mob', 'd', '26500', '', '0', '2013-08-17', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('236', '12', 'Cash at my home', 'd', '300000', '', '0', '2013-08-17', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('237', '12', 'Cash at my off', 'd', '200000', '', '0', '2013-08-19', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('241', '12', 'Cash at my home', 'd', '200000', '', '0', '2013-08-21', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('242', '12', 'Cash at my home', 'd', '200000', '', '0', '2013-08-21', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('243', '12', 'Cash at my shop', 'd', '200000', '', '0', '2013-08-23', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('247', '20', 'Comm', 'w', '255775', '', '0', '2013-08-25', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('248', '20', 'P.umesh', 'd', '100000', '', '0', '2013-08-25', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('253', '12', 'Comm', 'w', '83120', '', '0', '2013-08-25', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('258', '12', '25%', 'd', '40137', '', '0', '2013-08-25', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('260', '12', 'Cash at my shop', 'd', '143120', '', '0', '2013-08-26', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('265', '20', 'Comm', 'w', '141160', '', '0', '2013-08-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('270', '12', 'Comm', 'w', '102400', '', '0', '2013-08-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('273', '12', '25%', 'd', '184465', '', '0', '2013-08-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('276', '20', 'Cash at my home', 'd', '150000', '', '0', '2013-08-31', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('278', '12', 'Rajkot', 'd', '5000', '', '0', '2013-08-31', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('279', '20', 'Rajkot', 'd', '5000', '', '0', '2013-08-31', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('284', '12', 'Checq', 'd', '149200', '', '0', '2013-09-03', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('286', '12', 'Cash at my shop', 'd', '96080', '', '0', '2013-09-05', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('287', '12', 'Comm', 'd', '6320', '', '0', '2013-09-05', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('290', '3', 'Comm', 'd', '12400', '', '0', '2013-09-11', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('291', '20', 'Comm', 'd', '133900', '', '0', '2013-09-11', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('299', '3', 'Comm', 'd', '25050', '', '0', '2013-09-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('300', '20', 'Comm', 'w', '196035', '', '0', '2013-09-16', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('307', '12', 'Dt credit', 'd', '59200', '', '0', '2013-09-16', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('309', '12', 'Bhul', 'w', '5025', '', '0', '2013-09-06', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('321', '3', 'Come', 'd', '14910', '', '0', '2013-09-23', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('322', '20', 'Come', 'd', '457155', '', '0', '2013-09-23', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('325', '5', 'Come', 'w', '7200', '', '0', '2013-09-23', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('332', '12', 'Cash at my shop (harsh)', 'd', '50000', '', '0', '2013-09-25', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('334', '20', '3kg', 'w', '151126', '', '0', '2013-09-25', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('337', '3', 'Comm', 'd', '19630', '', '0', '2013-09-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('338', '3', 'Cash at my shop', 'w', '22000', '', '0', '2013-09-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('340', '20', 'Comm', 'd', '72890', '', '0', '2013-09-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('341', '20', 'Jc', 'w', '48000', '', '0', '2013-09-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('344', '5', 'Comm', 'd', '205900', '', '0', '2013-09-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('345', '5', 'In', 'w', '198700', '', '0', '2013-09-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('348', '12', 'Comm', 'w', '11700', '', '0', '2013-09-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('349', '12', '25%', 'd', '915', '', '0', '2013-09-30', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('354', '3', 'comm', 'w', '43120', '', '0', '2013-10-05', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('355', '20', 'comm', 'd', '149580', '', '0', '2013-10-05', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('358', '5', 'comm', 'w', '591500', '', '0', '2013-10-05', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('361', '12', '25%', 'd', '50000', '', '0', '2013-10-05', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('364', '12', 'cash at her off', 'd', '10785', '', '0', '2013-10-02', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('365', '2', 'cash unja', 'w', '4170', '', '0', '2013-10-05', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('368', '5', 'Ib', 'd', '500000', '', '0', '2013-10-07', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('370', '20', 'Dip bhav', 'w', '110000', '', '0', '2013-10-07', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('372', '20', 'Jc2bhav', 'd', '-90000', '', '0', '2013-10-07', 'Other', 'ClientToClient', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('374', '3', 'Comm', 'd', '32100', '', '0', '2013-10-14', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('375', '20', 'Comm', 'w', '618215', '', '0', '2013-10-14', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('378', '5', 'Comm', 'd', '314200', '', '0', '2013-10-14', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('383', '5', 'Ib', 'w', '222700', '', '0', '2013-10-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('384', '12', 'Md+cash at off', 'd', '25000', '', '0', '2013-10-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('385', '20', 'P.umesh', 'd', '300000', '', '0', '2013-10-15', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('388', '3', 'Comm', 'w', '9040', '', '0', '2013-10-24', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('389', '20', 'Comm', 'd', '649510', '', '0', '2013-10-24', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('390', '20', 'Ashish id', 'w', '317300', '', '0', '2013-10-24', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('393', '5', 'Comm', 'w', '101400', '', '0', '2013-10-24', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('394', '5', 'Cash', 'd', '101400', '', '0', '2013-10-24', 'Other', 'Cash', NULL);
insert into cashflow (cashFlowId, clientId, itemIdExpiryDate, dwStatus, dwAmount, plStatus, plAmount, transactionDate, transType, transMode, notes) values ('409', '3', 'Comm', 'd', '17320', '', '0', '2013-10-30', 'Other', 'Cash', NULL);
