<?php
include "./etc/om_config.inc";
session_start();
if(!isset($_SESSION['user'])
   || (isset($_SESSION['userType']) && isset($filesArr['bankMasterList'][$_SESSION['userType']]) 
        && $filesArr['bankMasterList'][$_SESSION['userType']] == 0))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:index.php");
}
else
{
  $smarty=new SmartyWWW();
  $selectBank = "SELECT * FROM bankmaster";
  $resultBank = mysql_query($selectBank);
  $i=0;
  while($rowBank = mysql_fetch_array($resultBank))
  {
  	$bankId[$i] = $rowBank['bankId'];
  	$bankName[$i] = $rowBank['bankName'];
  	$phone1[$i] = $rowBank['phone1'];
  	$phone2[$i] = $rowBank['phone2'];
  	$i++;
  }
  $smarty->assign("bankId",$bankId);
  $smarty->assign("bankName",$bankName);
  $smarty->assign("phone1",$phone1);
  $smarty->assign("phone2",$phone2);
  $smarty->assign("i",$i);
  $smarty->display("bankMasterList.tpl");
}  
?>