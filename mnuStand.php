<?php
session_start();
if(!isset($_SESSION['toDate']))
{
  $_SESSION['toDate'] = date("Y-M-d", mktime(0, 0, 0, date("m")  , date("d") - 1, date("Y")));
}

$toDateDay = substr($_SESSION['toDate'], 9, 2);
$toDateMonth = substr($_SESSION['toDate'], 5, 3);
$toDateYear = substr($_SESSION['toDate'], 0, 4);
$fileName = "fo".$toDateDay.strtoupper($toDateMonth).$toDateYear."bhav.csv.zip";  //fo06OCT2011bhav.csv.zip

?>
<HTML>
<BODY>
<A href="./index.php">Home</A><BR><BR>
<TABLE border="0" cellPadding="0" cellSpacing="4">
<TR><TD><B>Automatic : </B></TR><TR></TD><TD colspan="2">(You need to enter only item closing rates)</TD></TR>
<TR><TD>&nbsp</TD><TD><A href="bhavCopyFile.php">From BhavCopy</A></TD></TR>
<TR><TD>&nbsp</TD><TD><A href="standing.php">Click here if rates <B>not</B> entered</A></TD></TR>
<TR><TD>&nbsp</TD><TD><A href="standingNames.php">Click here if rates <B>already entered</B></A></TD></TR>
<TR><TD>&nbsp</TD><TD><A href="storeBhavCopyTxtFile.php?exc=MCX">Store Bhavcopy</B></A>
<A href="storeBhavCopyStand.php">Bhavcopy Standing</B></A>&nbsp;&nbsp;&nbsp;&nbsp;
<A href="bhavcopyMCX.php">Store Bhavcopy MCX</B></A> | 
<br /><br />
<?php
echo "<a href='http://www.nseindia.com/content/historical/DERIVATIVES/".$toDateYear."/".strtoupper($toDateMonth)."/".$fileName."'>Download Bhavcopy F_O</a>";
?>
&nbsp;&nbsp;&nbsp;&nbsp;
<A href="bhavcopyF_O.php">Store Bhavcopy F_O</B></A>	
</TD></TR>
<TR><TD><B>Manual : </B></TR><TR></TD><TD colspan="2">(You need to enter all information about standing trade)</TD></TR>
<TR><TD>&nbsp</TD><TD><A href="tradeAddMcx.php?forStand=1">Mcx manual standing</A></TD></TR>
<TR><TD>&nbsp</TD><TD><A href="tradeAddFO.php?forStand=1">F_O manual standing</A></TD></TR>
<TR><TD>&nbsp</TD><TD><A href="ajxStoreLimitTradeMcx1.php?useTpl=txtbx&forStand=1">New Mcx manual standing</A></TD></TR>
<TR><TD>&nbsp</TD><TD><A href="ajxStoreLimitTradeF_O1.php?useTpl=txtbx&forStand=1">New F_O manual standing</A></TD></TR>
</TABLE>
<BR><BR>
<A href="vendorTrade.php?forStand=1">Vendor Standing</A><BR><BR>
<!--<A href="vendorStand.php">Automatic</A><BR><BR>-->
<A href="standingList.php?forStand=1">StandingList</A><BR><BR>
</BODY>
</HTML>