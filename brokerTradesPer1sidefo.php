<?php
include "./etc/om_config.inc";
session_start();
function updateForGrossLine($trades, $i, $valuesForGrossLine)
{
  global $wholeItemArr, $wholeItemArrCount;
  $trades[$i-1]['dispGross']     = 1;
  $trades[$i-1]['totBuyQty']     = $valuesForGrossLine['totBuyQty'];
  $trades[$i-1]['totSellQty']    = $valuesForGrossLine['totSellQty'];
  $trades[$i-1]['totBuyAmount']  = $valuesForGrossLine['totBuyAmount'];
  $trades[$i-1]['totSellAmount'] = $valuesForGrossLine['totSellAmount'];
  $trades[$i-1]['buyRash']       = ($valuesForGrossLine['totBuyQty']!=0)?($valuesForGrossLine['totBuyAmount']/$valuesForGrossLine['totBuyQty']):0;
  $trades[$i-1]['sellRash']      = ($valuesForGrossLine['totSellQty']!=0)?($valuesForGrossLine['totSellAmount']/$valuesForGrossLine['totSellQty']):0;
  
  if($trades[$i-1]['totBuyQty'] == $trades[$i-1]['totSellQty'])
  {
    $trades[$i-1]['profitLoss']   = $valuesForGrossLine['totSellAmount'] - $valuesForGrossLine['totBuyAmount'];
    
    //We subtract openBuyQty and openSellQty... but actually there will be only 1 open qty : buy or sell ...so result will be ok
    $trades[$i-1]['oneSideBrok'] 
     = $valuesForGrossLine['oneSideBrok'] * $valuesForGrossLine['totBuyAmountOnly0'] / 100000
     + $valuesForGrossLine['oneSideBrok'] * $valuesForGrossLine['totSellAmountOnly0'] / 100000;
    
    $trades[$i-1]['netProfitLoss']= $trades[$i-1]['profitLoss'] - $trades[$i-1]['oneSideBrok'];
  }
  else
  {
    $trades[$i-1]['profitLoss']   = 0;
    $trades[$i-1]['oneSideBrok']  = 0;
    $trades[$i-1]['netProfitLoss']= 0;
  }
  
  $trades[$i-1]['clientTotProfitLoss'] = $valuesForGrossLine['clientPrevProfitLoss'] + $trades[$i-1]['profitLoss'];
  $trades[$i-1]['clientTotBrok']       = $valuesForGrossLine['clientPrevBrok'] + $trades[$i-1]['oneSideBrok'];
  $trades[$i-1]['clientTotNetProfitLoss'] = $trades[$i-1]['clientTotProfitLoss'] - $trades[$i-1]['clientTotBrok'];

  if(!array_search_recursive($trades[$i-1]['itemIdExpiry'], $wholeItemArr))
  {
    $wholeItemArrCount++;
    $wholeItemArr[$wholeItemArrCount]['itemIdExpiry']  = $trades[$i-1]['itemIdExpiry'];
    $wholeItemArr[$wholeItemArrCount]['buyQty']        = 0;
    $wholeItemArr[$wholeItemArrCount]['sellQty']       = 0;
    $wholeItemArr[$wholeItemArrCount]['totBuyAmount']  = 0;
    $wholeItemArr[$wholeItemArrCount]['totSellAmount'] = 0;
    $wholeItemArr[$wholeItemArrCount]['totBuyAmountOnly0']  = 0;
    $wholeItemArr[$wholeItemArrCount]['totSellAmountOnly0'] = 0;
    $wholeItemArr[$wholeItemArrCount]['profitLoss']    = 0;
    $wholeItemArr[$wholeItemArrCount]['oneSideBrok']   = 0;
    $wholeItemArr[$wholeItemArrCount]['netProfitLoss'] = 0;
    
    $elementNoToUse = $wholeItemArrCount;
  }
  else
  {
    $foundArray = array_search_recursive($trades[$i-1]['itemIdExpiry'], $wholeItemArr);
    $elementNoToUse = $foundArray[0];
  }
  
  $wholeItemArr[$elementNoToUse]['buyQty']        += $trades[$i-1]['totBuyQty'];
  $wholeItemArr[$elementNoToUse]['sellQty']       += $trades[$i-1]['totSellQty'];
  $wholeItemArr[$elementNoToUse]['totBuyAmount']  += $trades[$i-1]['totBuyAmount'];
  $wholeItemArr[$elementNoToUse]['totSellAmount'] += $trades[$i-1]['totSellAmount'];
  $wholeItemArr[$elementNoToUse]['buyRash']       = ($wholeItemArr[$wholeItemArrCount]['buyQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totBuyAmount']/$wholeItemArr[$wholeItemArrCount]['buyQty']):0;
  $wholeItemArr[$elementNoToUse]['sellRash']      = ($wholeItemArr[$wholeItemArrCount]['sellQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totSellAmount']/$wholeItemArr[$wholeItemArrCount]['sellQty']):0;
  $wholeItemArr[$elementNoToUse]['profitLoss']    += $trades[$i-1]['profitLoss'];
  $wholeItemArr[$elementNoToUse]['oneSideBrok']   += $trades[$i-1]['oneSideBrok'];
  $wholeItemArr[$elementNoToUse]['netProfitLoss'] += $trades[$i-1]['netProfitLoss'];

  return $trades;
}
if(!isset($_SESSION['user'])
   || (isset($_SESSION['userType']) && isset($filesArr['brokerTradesMcx'][$_SESSION['userType']]) 
        && $filesArr['brokerTradesMcx'][$_SESSION['userType']] == 0))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:index.php");
}
else
{
  $goTo = "brokerTradesPer1sidefo";
  $under100BrokDiff = true;
  $edit1File = "ajxEditLimitTradeFO.php";
  $edit2File = "ajxEditLimitTradeFO1.php";
  if(!isset($_SESSION['toDate'])) 
  {
    header("Location: selectDtSession.php?goTo=".$goTo);
  }
  else
  {
    include "./etc/functions.inc";
    $smarty = new SmartyWWW();
    
    $message = "";
    
  ////Request parameters, if passed : transfer to proper variable :Start
    if(isset($_GET['display']))
      $display = $_GET['display'];
    else
      $display = 'trades';
    if(isset($_GET['itemId']))
      $currentItemId = $_GET['itemId'];
    else
      $currentItemId = "All";
  ////Request parameters, if passed : transfer to proper variable :End
    
    //Client records :Start
    $groupIdSelected = isset($_REQUEST['groupId']) ? $_REQUEST['groupId'] : 0;
    $clientIdSelected = isset($_GET['clientId'])?$_GET['clientId']:0;
    $clientIdValues = array();
    $clientIdOptions = array();
    $clientInfo      = array();
    $i = 0;
    $clientIdValues[0]  = 0;
    $clientIdOptions[0] = 'All';
    $i++;
  
    $clientQuery = "SELECT * FROM client 
                      WHERE clientBroker = 1
                      ORDER BY firstName, middleName, lastName";
    $clientResult = mysql_query($clientQuery);
    while($clientRow = mysql_fetch_array($clientResult))
    {
      $clientIdValues[$i] = $clientRow['clientId'];
      $clientIdOptions[$i] = $clientRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];
      
      $clientInfo[$clientRow['clientId']]['deposit'] = $clientRow['deposit'];
      $i++;
    }
    //Client records :End
    //Item records :Start
    $itemIdSelected = $currentItemId;
    $itemIdValues = array();
    $itemIdOptions = array();
    $itemCount = 0;
    $itemIdValues[0]  = "All";
    $itemIdOptions[0] = "All";
    $itemCount++;
  
    $itemRecords = array();
    $clientBrok  = array();
    $itemQuery = "SELECT * FROM item ORDER BY itemId";
    $itemResult = mysql_query($itemQuery);
    while($itemRow = mysql_fetch_array($itemResult))
    {
      $itemRecords[$itemRow['itemId']]['priceOn'] = $itemRow['priceOn'];
      $itemRecords[$itemRow['itemId']]['min']     = $itemRow['min'];
      
      //ClientBrok :Start
      for($i=0;$i<count($clientIdValues);$i++)
        $clientBrok[$clientIdValues[$i]][$itemRow['itemId']] = $itemRow['oneSideBrok'];
      //ClientBrok :End
      
      $itemIdValues[$itemCount]  = $itemRow['itemId'];
      $itemIdOptions[$itemCount] = $itemRow['itemId'];
      $itemCount++;
    }
    //Item records :End
    //Override clientBrok from clientBrok table :Start
    $clientBrokQuery = "SELECT * FROM clientbrok ORDER BY clientId";
    $clientBrokResult = mysql_query($clientBrokQuery);
    while($clientBrokRow = mysql_fetch_array($clientBrokResult))
    {
      $clientBrok[$clientBrokRow['clientId']][$clientBrokRow['itemId']] = $clientBrokRow['oneSideBrok'];
    }
    //Override clientBrok from clientBrok table :Start
    
    //Expiry records :Start
    if(isset($_GET['expiryDate']))
    {
      if($_GET['itemIdChanged']==1 || $currentItemId=="All")
        $expiryDateSelected = 0;
      else
        $expiryDateSelected = $_GET['expiryDate'];
    }
    else
      $expiryDateSelected = 0;
      
    $expiryDateValues = array();
    $expiryDateOptions = array();
    $i = 0;
    $expiryDateValues[0]  = 0;
    $expiryDateOptions[0] = 'All';
    $i++;
  
    if($currentItemId!="All")
    {
      $expiryQuery = "SELECT * FROM expiry
                        ORDER BY itemId, expiryDate";
      $expiryResult = mysql_query($expiryQuery);
      while($expiryRow = mysql_fetch_array($expiryResult))
      {
        if($expiryRow['itemId'] == $currentItemId)
        {
          $expiryDateValues[$i]  = $expiryRow['expiryDate'];
          $expiryDateOptions[$i] = $expiryRow['expiryDate'];
          $i++;
        }
      }
    }
    //Expiry records :End
  
    $trades = array();
    $prevClientId = 0;
    $prevItemId = '';
    $prevExpiryDate = '';
    $valuesForGrossLine = array();
    $wholeItemArr       = array();
    $wholeItemArrCount  = -1;  //-1, because we do ++ when we store 0
  
    $wholeBuyQty        = 0;
    $wholeTotBuyAmount  = 0;
    $wholeBuyRash       = 0;
    $wholeSellQty       = 0;
    $wholeTotSellAmount = 0;
    $wholeSellRash      = 0;
    $wholeProfitLoss    = 0;
    $wholeOneSideBrok   = 0;
    $wholeNetProfitLoss = 0;
    $wholeNetLossOnly   = 0;
    $wholeNetProfitOnly = 0;
  
    $valuesForGrossLine['openBuyQty'] = 0;
    $valuesForGrossLine['openSellQty'] = 0;
    $valuesForGrossLine['totBuyQty'] = 0;
    $valuesForGrossLine['totSellQty'] = 0;
    $valuesForGrossLine['totBuyAmount']       = 0;
    $valuesForGrossLine['totSellAmount']      = 0;
    $valuesForGrossLine['totBuyAmountOnly0']  = 0;
    $valuesForGrossLine['totSellAmountOnly0'] = 0;
    $i = 0;
    $tradesQuery = "SELECT * FROM tradetxt
                    WHERE 1=1";
  /////////////////////////////////////////////Where Condition :Start
    $whereGiven = true;
    if($groupIdSelected != 0)
    {
      $tradesQuery .= " AND clientId IN (SELECT clientId FROM client WHERE groupId = ".$groupIdSelected.") ";
    }
    if(isset($_GET['clientId']) && $_GET['clientId']!=0)
    {
      $tradesQuery .= " AND clientId = ".$_GET['clientId'];
      $whereGiven = true;
    }

    if($whereGiven)
      $tradesQuery .= " AND   exchange LIKE 'F_O'";
    else
      $tradesQuery .= " WHERE exchange LIKE 'F_O'";
    $whereGiven = true;

    if($currentItemId!="All")
    {
      if($whereGiven)
        $tradesQuery .= " AND   itemId LIKE '".$currentItemId."'";
      else
        $tradesQuery .= " WHERE itemId LIKE '".$currentItemId."'";
      $whereGiven = true;
    }
    if(isset($_GET['expiryDate']) && $_GET['expiryDate']!='0' && $_GET['itemIdChanged']!=1 && $currentItemId!="All")
    {
      if($whereGiven)
        $tradesQuery .= " AND   expiryDate LIKE '".$_GET['expiryDate']."'";
      else
        $tradesQuery .= " WHERE expiryDate LIKE '".$_GET['expiryDate']."'";
      $whereGiven = true;
    }
  
    if(isset($_SESSION['fromDate']))
    {//WHERE tradeDate >=  '2004-08-03' AND tradeDate <=  '2004-08-04'
      if($whereGiven)
       $tradesQuery .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
      else
      {
        $tradesQuery .= " WHERE tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
        $whereGiven = true;
      }
    }
  
    if($display == 'openStand')
    {
      if($whereGiven)
       $tradesQuery .= " AND standing = -1";
      else
      {
        $tradesQuery .= " WHERE standing = -1";
        $whereGiven = true;
      }
    }
    if($display == 'closeStand')
    {
      if($whereGiven)
       $tradesQuery .= " AND standing = 1";
      else
      {
        $tradesQuery .= " WHERE standing = 1";
        $whereGiven = true;
      }
    }
    if($display == 'openCloseStand')
    {
      if($whereGiven)
       $tradesQuery .= " AND (standing = -1 OR standing = 1)";
      else
      {
        $tradesQuery .= " WHERE (standing = -1 OR standing = 1)";
        $whereGiven = true;
      }
    }
    
    if($display == 'noInnerStand')
    {
      if($whereGiven)
        $tradesQuery .= " AND NOT ((standing = -1 OR standing = 1) 
                         AND tradeDate != '".$_SESSION['fromDate']."'
                         AND tradeDate != '".$_SESSION['toDate']."')";
      else
      {
        $tradesQuery .= " WHERE NOT ((standing = -1 OR standing = 1) 
                         AND tradeDate != '".$_SESSION['fromDate']."'
                         AND tradeDate != '".$_SESSION['toDate']."')";
        $whereGiven = true;
      }
    }
  /////////////////////////////////////////////Where Condition :End
    $tradesQuery .= " AND clientId IN (SELECT clientId FROM client 
                                   WHERE clientBroker = 1)";

    $tradesQuery .= " ORDER BY firstName, middleName, lastName, itemId ASC, expiryDate, 
                       tradeDate ASC, standing, tradeTime, tradeId";
    $tradesResult = mysql_query($tradesQuery);
    if(mysql_num_rows($tradesResult) == 0)
      $message = "No records!";
    else
    {
      while($tradesRow = mysql_fetch_array($tradesResult))
      {
        $trades[$i]['tradeId']         = $tradesRow['tradeId'];
        $trades[$i]['dispGross']       = 0;
        $trades[$i]['dispClientWhole'] = 0;
        
        if($i==0)
        { $valuesForGrossLine['clientPrevProfitLoss'] = 0;  $valuesForGrossLine['clientPrevBrok'] = 0;  }
    
        //For Gross line :Start //Gross section comes initially in while loop, but it is used after displaying trades ... it is here because when client or item or expiry change, first we store data to display gross for previous matter
        if($tradesRow['clientId'] != $prevClientId || $tradesRow['itemId'] != $prevItemId || $tradesRow['expiryDate'] != $prevExpiryDate)
        {
          if($prevClientId != 0)
          {
            $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
            $valuesForGrossLine['clientPrevProfitLoss'] = $trades[$i-1]['clientTotProfitLoss'];
            $valuesForGrossLine['clientPrevBrok']       = $trades[$i-1]['clientTotBrok'];
            
            $valuesForGrossLine['openBuyQty'] = 0;
            $valuesForGrossLine['openSellQty'] = 0;
            $valuesForGrossLine['totBuyQty']  = 0;
            $valuesForGrossLine['totSellQty'] = 0;
            $valuesForGrossLine['totBuyAmount'] = 0;
            $valuesForGrossLine['totSellAmount'] = 0;
            $valuesForGrossLine['totBuyAmountOnly0'] = 0;
            $valuesForGrossLine['totSellAmountOnly0'] = 0;
            
            if($tradesRow['clientId'] != $prevClientId)
            {
              $trades[$i-1]['dispClientWhole'] = 1;
              $valuesForGrossLine['clientPrevProfitLoss'] = 0;  $valuesForGrossLine['clientPrevBrok'] = 0;
            }
          }
        }
        //For Gross line :End
        //priceOn and min are here because we need to assign value after gross line, and also, at first time : we take first priceOn and min and then we use it in gross line
        $priceOn = $itemRecords[$tradesRow['itemId']]['priceOn'];
        $valuesForGrossLine['min'] = $itemRecords[$tradesRow['itemId']]['min'];
    
        //We take first oneSideBrok and then use in Gross line
        $valuesForGrossLine['oneSideBrok'] = $clientBrok[$tradesRow['clientId']][$tradesRow['itemId']];
        
        $trades[$i]['clientId']     = $tradesRow['clientId'];
        $trades[$i]['prevClientId'] = $prevClientId;
        $prevClientId               = $tradesRow['clientId'];
        $trades[$i]['itemId']       = $tradesRow['itemId'];
        $trades[$i]['prevItemId']   = $prevItemId;
        $prevItemId                 = $tradesRow['itemId'];
        $trades[$i]['expiryDate']   = $tradesRow['expiryDate'];
        $trades[$i]['prevExpiryDate']   = $prevExpiryDate;
        $prevExpiryDate             = $tradesRow['expiryDate'];
    
        $trades[$i]['clientName'] = $tradesRow['firstName']." ".$tradesRow['middleName']." ".$tradesRow['lastName'];
        $trades[$i]['clientDeposit'] = $clientInfo[$tradesRow['clientId']]['deposit'];
        $trades[$i]['vendor']    = $tradesRow['vendor'];
        $trades[$i]['tradeDate'] = mysqlToDDMMYY($tradesRow['tradeDate']);
        $trades[$i]['tradeTime'] = $tradesRow['tradeTime'];
        $trades[$i]['standing']  = standToDisplay($tradesRow['standing']);
        $trades[$i]['itemIdExpiry'] = $tradesRow['itemId']."-".substr($tradesRow['expiryDate'],2,3);
        $trades[$i]['buySell']   = $tradesRow['buySell'];
    
        $trades[$i]['tradeRefNo']  = $tradesRow['tradeRefNo'];
        $trades[$i]['userRemarks'] = $tradesRow['userRemarks'];
        $trades[$i]['ownClient']   = $tradesRow['ownClient'];
    
        if($tradesRow['buySell'] == 'Buy')
        {
          $trades[$i]['fontColor'] = "blue";
          $trades[$i]['buyQty']    = $tradesRow['qty'];
          $trades[$i]['price']     = $tradesRow['price'];
          $trades[$i]['sellQty']   = '&nbsp;';
          $trades[$i]['sellPrice'] = '&nbsp;';
          
          if($tradesRow['standing'] == -1)
            $valuesForGrossLine['openBuyQty'] += $tradesRow['qty'];
            
          $valuesForGrossLine['totBuyQty'] += $tradesRow['qty'];
          $valuesForGrossLine['totBuyAmount'] += $tradesRow['price']*$tradesRow['qty'];
          if($tradesRow['standing'] == 0)
            $valuesForGrossLine['totBuyAmountOnly0'] += $tradesRow['price']*$tradesRow['qty'];
        }
        else
        {
          $trades[$i]['fontColor'] = "red";
          $trades[$i]['buyQty'] = '&nbsp;';
          $trades[$i]['price'] = '&nbsp;';
          $trades[$i]['sellQty'] = $tradesRow['qty'];
          $trades[$i]['sellPrice'] = $tradesRow['price'];
          
          if($tradesRow['standing'] == -1)
            $valuesForGrossLine['openSellQty'] += $tradesRow['qty'];
            
          $valuesForGrossLine['totSellQty'] += $tradesRow['qty'];
          $valuesForGrossLine['totSellAmount'] += $tradesRow['price']*$tradesRow['qty'];
          if($tradesRow['standing'] == 0)
            $valuesForGrossLine['totSellAmountOnly0'] += $tradesRow['price']*$tradesRow['qty'];
        }
        $i++;
      }
      $totTrades = $i;
      
      $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
      $trades[$i-1]['dispClientWhole'] = 1;
  
      //formatInIndianStyle :Start
      for($i=0;$i<$totTrades;$i++)
      {
        $trades[$i]['totBuyAmount']  = formatInIndianStyle(isset($trades[$i]['totBuyAmount'])?$trades[$i]['totBuyAmount']:0);
        $trades[$i]['totSellAmount'] = formatInIndianStyle(isset($trades[$i]['totSellAmount'])?$trades[$i]['totSellAmount']:0);
        $trades[$i]['buyRash']       = number_format(isset($trades[$i]['buyRash'])?$trades[$i]['buyRash']:0,0,'.','');
        $trades[$i]['sellRash']      = number_format(isset($trades[$i]['sellRash'])?$trades[$i]['sellRash']:0,0,'.','');
        $trades[$i]['profitLoss']    = formatInIndianStyle(isset($trades[$i]['profitLoss'])?$trades[$i]['profitLoss']:0);
        $trades[$i]['oneSideBrok']   = formatInIndianStyle(isset($trades[$i]['oneSideBrok'])?$trades[$i]['oneSideBrok']:0);
        $trades[$i]['netProfitLossNotFormatted'] = isset($trades[$i]['netProfitLoss'])?$trades[$i]['netProfitLoss']:0;
        $trades[$i]['netProfitLoss'] = formatInIndianStyle(isset($trades[$i]['netProfitLoss'])?$trades[$i]['netProfitLoss']:0);
        
        $trades[$i]['clientTotProfitLoss'] = formatInIndianStyle(isset($trades[$i]['clientTotProfitLoss'])?$trades[$i]['clientTotProfitLoss']:0);
        $trades[$i]['clientTotBrok']       = formatInIndianStyle(isset($trades[$i]['clientTotBrok'])?$trades[$i]['clientTotBrok']:0);
        $trades[$i]['clientTotNetProfitLossNoFormat'] = isset($trades[$i]['clientTotNetProfitLoss'])?$trades[$i]['clientTotNetProfitLoss']:0;
        $trades[$i]['clientTotNetProfitLoss'] = formatInIndianStyle(isset($trades[$i]['clientTotNetProfitLoss'])?$trades[$i]['clientTotNetProfitLoss']:0);
      }
      for($i=0;$i<=$wholeItemArrCount;$i++)
      {
    //////////////For whole total :Start
        $wholeBuyQty        += $wholeItemArr[$i]['buyQty'];
        $wholeTotBuyAmount  += $wholeItemArr[$i]['totBuyAmount'];
        $wholeSellQty       += $wholeItemArr[$i]['sellQty'];
        $wholeTotSellAmount += $wholeItemArr[$i]['totSellAmount'];
        $wholeProfitLoss    += $wholeItemArr[$i]['profitLoss'];
        $wholeOneSideBrok   += $wholeItemArr[$i]['oneSideBrok'];
        $wholeNetProfitLoss += $wholeItemArr[$i]['netProfitLoss'];
        if($wholeItemArr[$i]['netProfitLoss'] >= 0) $wholeNetProfitOnly += $wholeItemArr[$i]['netProfitLoss'];
        else $wholeNetLossOnly += $wholeItemArr[$i]['netProfitLoss'];
  
    //////////////For whole total :End
    //////////////For number_format :Start
        $wholeItemArr[$i]['totBuyAmount']  = formatInIndianStyle($wholeItemArr[$i]['totBuyAmount']);
        $wholeItemArr[$i]['totSellAmount'] = formatInIndianStyle($wholeItemArr[$i]['totSellAmount']);
        $wholeItemArr[$i]['buyRash']       = number_format($wholeItemArr[$i]['buyRash'],0,'.','');
        $wholeItemArr[$i]['sellRash']      = number_format($wholeItemArr[$i]['sellRash'],0,'.','');
        $wholeItemArr[$i]['profitLoss']    = formatInIndianStyle($wholeItemArr[$i]['profitLoss']);
        $wholeItemArr[$i]['oneSideBrok']   = formatInIndianStyle($wholeItemArr[$i]['oneSideBrok']);
        $wholeItemArr[$i]['netProfitLoss'] = formatInIndianStyle($wholeItemArr[$i]['netProfitLoss']);
    //////////////For number_format :End
      }
      $wholeBuyRash  = ($wholeBuyQty!=0)?($wholeTotBuyAmount/$wholeBuyQty):0;
      $wholeSellRash = ($wholeSellQty!=0)?($wholeTotSellAmount/$wholeSellQty):0;
    }
    //formatInIndianStyle :End
    
    $smarty->assign("PHP_SELF", $_SERVER['PHP_SELF']);
    $smarty->assign("goTo", $goTo);
    $smarty->assign("edit1File", $edit1File);
    $smarty->assign("edit2File", $edit2File);
    $smarty->assign("display", $display);
    $smarty->assign("message", $message);
    $smarty->assign("clientIdSelected", $clientIdSelected);
    $smarty->assign("clientIdValues",   $clientIdValues);
    $smarty->assign("clientIdOptions",  $clientIdOptions);
    $smarty->assign("itemIdSelected",   $itemIdSelected);
    $smarty->assign("itemIdValues",     $itemIdValues);
    $smarty->assign("itemIdOptions",    $itemIdOptions);
    $smarty->assign("expiryDateSelected", $expiryDateSelected);
    $smarty->assign("expiryDateValues",   $expiryDateValues);
    $smarty->assign("expiryDateOptions",  $expiryDateOptions);
  
    $smarty->assign("fromDate", substr($_SESSION['fromDate'],8,2)."-".substr($_SESSION['fromDate'],5,2)."-".substr($_SESSION['fromDate'],2,2));
    $smarty->assign("toDate",   substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],2,2));
    $smarty->assign("profitLossDate", substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],0,4));
    
    $smarty->assign("trades",           $trades);
    $smarty->assign("wholeItemArr",     $wholeItemArr);
  
    $smarty->assign("wholeBuyQty",        $wholeBuyQty);
    $smarty->assign("wholeTotBuyAmount",  formatInIndianStyle($wholeTotBuyAmount));
    $smarty->assign("wholeBuyRash",       number_format($wholeBuyRash,0,'.',''));
    $smarty->assign("wholeSellQty",       $wholeSellQty);
    $smarty->assign("wholeTotSellAmount", formatInIndianStyle($wholeTotSellAmount));
    $smarty->assign("wholeSellRash",      number_format($wholeSellRash,0,'.',''));
    $smarty->assign("wholeProfitLoss",    formatInIndianStyle($wholeProfitLoss));
    $smarty->assign("wholeOneSideBrok",   formatInIndianStyle($wholeOneSideBrok));
    $smarty->assign("wholeNetProfitLoss", formatInIndianStyle($wholeNetProfitLoss));
    $smarty->assign("wholeNetProfitOnly", formatInIndianStyle($wholeNetProfitOnly));
    $smarty->assign("wholeNetLossOnly",   formatInIndianStyle($wholeNetLossOnly));
  
////////////////////////
    $groupIds   = array();
    $groupNames = array();
    $groupQuery = "SELECT * FROM groups"
      . " ORDER BY groupName";
    $groupResult = mysql_query($groupQuery);
    $i = 0;
    $groupIds[$i]   = 0;
    $groupNames[$i] = "All";
    $i++;
    while($groupRow = mysql_fetch_array($groupResult))
    {
      $groupIds[$i]   = $groupRow['groupId'];
      $groupNames[$i] = $groupRow['groupName'];
      $i++;
    }
    $smarty->assign("groupIdSelected", $groupIdSelected);
    $smarty->assign("groupIds",        $groupIds);
    $smarty->assign("groupNames",      $groupNames);
////////////////////////
    
  /////////////////Use tpl as per 'display' parameter :Start
    if($display == 'trades')
      $smarty->display("clientTrades.tpl");
    elseif($display == 'detailed')
      $smarty->display("clientTrades.tpl");//We display detailed view from same file for minimum view
    elseif($display == 'gross')
      $smarty->display("clientGross.tpl");
    elseif($display == 'itemWiseGross')
      $smarty->display("clientItemWiseGross.tpl");
    elseif($display == 'itemPending')
      $smarty->display("clientItemPending.tpl");
    elseif($display == 'openStand' || $display == 'closeStand' || $display == 'openCloseStand')
      $smarty->display("clientStand.tpl");
    elseif($display == 'PLToAccount')
      $smarty->display("clientPLToAccount.tpl");
    elseif($display == 'tradesPrint')
      $smarty->display("clientTradesPrint.tpl");
    elseif($display == 'tradesPrint2')
      $smarty->display("clientTradesPrint2.tpl");
    elseif($display == 'grossPrint')
      $smarty->display("clientGrossPrint.tpl");
    else
      $smarty->display("clientTrades.tpl");
  /////////////////Use tpl as per 'display' parameter :End
  }
  ///////////////////////////////
  if($billWithLedger == "1")
  {
    $comboClientId = isset($_GET['clientId'])?$_GET['clientId']:0;
    include "./accTransList3.php";
  }
}
?>