<?php
  include "etc/om_config.inc";
  
  $smarty = new SmartyWWW();
  
/////////////////////////////
  $orderTypeValues = array();
  $orderTypeOutput = array();
  $orderValidityValues = array();
  $orderValidityOutput = array();
  $orderTypeValues[0] = 'RL';
  $orderTypeValues[1] = 'SL';
  $orderTypeOutput[0] = 'RL';
  $orderTypeOutput[1] = 'SL';
  $orderValidityValues[0] = 'EOS';
  $orderValidityValues[1] = 'GTD';
  $orderValidityValues[2] = 'GTC';
  $orderValidityOutput[0] = 'EOS';
  $orderValidityOutput[1] = 'GTD';
  $orderValidityOutput[2] = 'GTC';
  
/////////////////////////////
  
////////////////////////////////  
  if(isset($_GET['orderId']))
  {
    $orderSelectQuery = "SELECT * FROM orders
                         WHERE orderId = ".$_GET['orderId'];
    $orderSelectResult = mysql_query($orderSelectQuery);
    while($orderSelectRow = mysql_fetch_array($orderSelectResult))
    {
      $orderTypeSelected = $orderSelectRow['orderType'];
      $orderDateSelected = $orderSelectRow['orderDate'];
      $orderItemIdSelected = $orderSelectRow['itemId'];
      $orderExpiryDateSelected = $orderSelectRow['expiryDate'];
      $orderQtySelected = $orderSelectRow['qty'];
      $orderPriceSelected = $orderSelectRow['price'];
      $orderTriggerPriceSelected = $orderSelectRow['triggerPrice'];
      $orderValiditySelected = $orderSelectRow['orderValidity'];
      $orderBuySellSelected = $orderSelectRow['buySell'];
      $orderValidTillDateSelected = $orderSelectRow['orderValidTillDate'];
      $orderUserRemarkSelected = $orderSelectRow['clientId'];
      $orderVendor = $orderSelectRow['vendor'];
    }
    
    //////////////For Order Vendor:Start
    $vendorQuery = "SELECT * FROM vendor
                    WHERE vendor LIKE '".$orderVendor."'";
    $vendorResult = mysql_query($vendorQuery);
    while($vendorRow = mysql_fetch_array($vendorResult))
    {
      $vendorId = $vendorRow['vendorId'];
    }
    //////////////For Order Vendor:End
    
    $smarty->assign("orderTypeSelected",$orderTypeSelected);
    $smarty->assign("orderDateSelected",$orderDateSelected);
    $smarty->assign("orderItemIdSelected",$orderItemIdSelected);
    $smarty->assign("orderExpiryDateSelected",$orderExpiryDateSelected);
    $smarty->assign("orderQtySelected",$orderQtySelected);
    $smarty->assign("orderPriceSelected",$orderPriceSelected);
    $smarty->assign("orderTriggerPriceSelected",$orderTriggerPriceSelected);
    $smarty->assign("orderValiditySelected",$orderValiditySelected);
    $smarty->assign("orderBuySellSelected",$orderBuySellSelected);
    $smarty->assign("orderUserRemarkSelected",$orderUserRemarkSelected);
    $smarty->assign("orderValidityOutput",$orderValidityOutput);
    $smarty->assign("orderValidityValues",$orderValidityValues);
    $smarty->assign("orderIdSelected",$_GET['orderId']);
    $smarty->assign("orderValidTillDateSelected",$orderValidTillDateSelected);
    $smarty->assign("vendorId",$vendorId);
  }
  else
  {
    $orderTypeSelected = '';
  }
////////////////////////////////  
  
  if (isset($_POST['makeTrade']) && $_POST['makeTrade'] == 1)
  {
    $orderStatus = 'Pending';
////////////////////////
/////////////////////////////////////
    $clientRecordFound = 0;
    if(isset($_POST['clientId']) && $_POST['clientId'] > 0)
    {
      $selectClientQuery = "SELECT * FROM client
                            WHERE clientId = ".$_POST['clientId']."
                           ";
      $selectClientResult = mysql_query($selectClientQuery);
      
      if($selectClientRow = mysql_fetch_array($selectClientResult))
      {
        $clientRecordFound = 1;
        $clientId   = $_POST['clientId'];
        $firstName  = $selectClientRow['firstName'];
        $middleName = $selectClientRow['middleName'];
        $lastName   = $selectClientRow['lastName'];
        $clientWholeName = $selectClientRow['firstName']." ".$selectClientRow['middleName']." ".$selectClientRow['lastName'];
      }
      else
        $clientRecordFound = 0;
    }
    else
      $clientRecordFound = 0;
      
    if($clientRecordFound == 0)
    {
      $clientId   = 0;
      $firstName  = '';
      $middleName = '';
      $lastName   = '';
      $clientWholeName = '';
    }
/////////////////////////////////////
    
////////////////////////
    if(isset($_POST['clientId']))
      $userRemarks = $_POST['clientId'];
    else
      $userRemarks = '';
////////////////////////
////////////////////////
    $tradeDate  = $_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay'];

    if(isset($_POST['expiryDate']) && strlen($_POST['expiryDate']) > 0)
      $expiryDate = $_POST['expiryDate'];
    else
      $expiryDate = "";
    
    $standing = isset($_POST['standing'])?$_POST['standing']:"0";
    
    $currentDateWithTime = getdate();
    if(isset($_POST['tradeBtn']))
    {
      $insertQuery  = "INSERT INTO tradetxt 
                         (standing,clientId,firstName,middleName,lastName,
                          buySell,itemId,tradeDate,tradeTime,qty,price,expiryDate,vendor,userRemarks)
                      VALUES ('".$standing."', '".$clientId."', 
                              '".$firstName."',
                              '".$middleName."',
                              '".$lastName."',
                              '".$_POST['buySell']."',
                              '".$_POST['itemId']."', '".$tradeDate."','".date("H:i:s",time())."',
                              '".$_POST['qty']."', '".$_POST['price']."',
                              '".$expiryDate."', '".$_POST['vendor']."','".$userRemarks."'
                             )";
      $result = mysql_query($insertQuery);
      if(!$result)
        echo mysql_error()."<BR>".$insertQuery;
        
      $orderStatus = 'Executed';
    }
    $vendor = "_SELF";
    if($_POST['vendorId'] > 0)
    {
      $vendorIdQuery = "SELECT * FROM vendor
                        WHERE vendorId = ".$_POST['vendorId'];
      $vendorResult = mysql_query($vendorIdQuery);
      while($vendorRow = mysql_fetch_array($vendorResult))
        $vendor = $vendorRow['vendor'];
    }
      
      $ordersQuery  = "UPDATE orders SET
                       clientId='".$clientId."',
                       firstName='".$firstName."',
                       middleName='".$middleName."',
                       lastName='".$lastName."',
                       buySell='".$_POST['buySell']."',
                       itemId='".$_POST['itemId']."',
                       orderDate='".$tradeDate."',
                       orderTime='".date("H:i:s",time())."',
                       qty='".$_POST['qty']."',
                       price='".$_POST['price']."',
                       triggerPrice='".$_POST['triggerPrice']."',
                       expiryDate='".$expiryDate."',
                       vendor='".$vendor."',
                       orderType='".$_POST['orderType']."',
                       orderValidity='".$_POST['orderValidity']."',
                       orderStatus='".$orderStatus."',
                       userRemarks='".$userRemarks."'
                       WHERE orderId='".$_POST['orderId']."'";
                                     
      $ordersResult = mysql_query($ordersQuery);
      if(!$ordersResult)
        echo mysql_error()."<BR>".$ordersQuery;

    echo "<SCRIPT language='JavaScript'>window.close();</SCRIPT>";
  }
  
///////////////////////////////////////////////////////
  $firstName  = '';
  $middleName = '';
  $lastName   = '';
  $selectQuery = "SELECT * FROM client
                  ORDER BY firstName, middleName, lastName
                 ";
  if(isset($_POST['clientId']))
    $currentClientId = $_POST['clientId'];
  else
    $currentClientId = 0;
  
  $result = mysql_query($selectQuery);
  
  $clientIdValues = array();
  $clientIdOutput = array();
  $i = 0;
  while($row = mysql_fetch_array($result))
  {
    if($currentClientId == 0)
    {
      $currentClientId = $row['clientId'];
      $clientIdSelected = $row['clientId'];
      $firstName  = $row['firstName'];
      $middleName = $row['middleName'];
      $lastName   = $row['lastName'];
      $clientWholeName = $row['firstName']." ".$row['middleName']." ".$row['lastName'];
    }
    
    if($row['clientId'] == $currentClientId)
    {
      $clientIdSelected = $row['clientId'];
      $firstName  = $row['firstName'];
      $middleName = $row['middleName'];
      $lastName   = $row['lastName'];
      $clientWholeName = $row['firstName']." ".$row['middleName']." ".$row['lastName'];
      //////////////////////////////////////////////
      $currentBal = $row['currentBal'];
      $deposit = $row['deposit'];
      $phone   = $row['phone'];
      $mobile  = $row['mobile'];
      //////////////////////////////////////////////
    }
    
    $clientIdValues[$i] = $row['clientId'];
    $clientIdOutput[$i] = $row['firstName']." ".$row['middleName']." ".$row['lastName'];
    $i++;
  }
///////////////////////////////////////////////////////

  if(count($clientIdValues) > 0)
  {
    if(isset($_POST['price']))
      $lastPrice = $_POST['price'];
    else
      $lastPrice = '';
  
    if(isset($_POST['buySell']))
      $buySellSelected  = $_POST['buySell'];
    else
      $buySellSelected  = "Buy";
    $buySellValues = array("Buy", "Sell");
    $buySellOutput = array("Buy", "Sell");
  ////    
    if(isset($_POST['tradeDay']))
      $tradeDateDisplay = $_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."-";
    else
      $tradeDateDisplay = date("Y-m-d");
  
  ///////////////////////////////////////////////////////
    $minQty = 0;
    $selectItemQuery = "SELECT * FROM item ORDER BY itemId";
    if(isset($_POST['itemId']))
      $currentItemId = $_POST['itemId'];
    else
      $currentItemId = '';
    
    $itemResult = mysql_query($selectItemQuery);
    
    $i = 0;
    $itemIdSelected = '';
    $itemIdValues = array();
    $itemIdOutput = array();
    $itemFromPriceJS = '';
    while($itemRow = mysql_fetch_array($itemResult))
    {
      if($currentItemId == '')
        $currentItemId = $itemRow['itemId'];
      
      if($itemRow['itemId'] == $currentItemId)
      {
        $itemIdSelected = $itemRow['itemId'];
        $minQty = $itemRow['min'];
      }
      
      $itemIdValues[$i] = $itemRow['itemId'];
      $itemIdOutput[$i] = $itemRow['itemId'];
      
      //For JavaScript of itemFromPrice :Start
      if(!is_null($itemRow['rangeStart']) && !is_null($itemRow['rangeEnd']))
      {
        $itemFromPriceJS .= "  if(price >= ".$itemRow['rangeStart']." && price <= ".$itemRow['rangeEnd'].")\n";
        $itemFromPriceJS .= "  {\n";
        $itemFromPriceJS .= "    document.form1.itemId.selectedIndex = ".$i.";\n";
        $itemFromPriceJS .= "    changeItem();\n";
        $itemFromPriceJS .= "  }\n";
      }
      //For JavaScript of itemFromPrice :End
      $i++;
    }
  /////////////////////////////////////////////
  /////////////////////////////////////////////
    $expiryQuery = "SELECT * FROM expiry
                      WHERE itemId = '".$currentItemId."'";
    $expiryResult = mysql_query($expiryQuery);
    
    $i = 0;
    $expiryDateSelected = '';
    $expiryDateValues  = array();
    $expiryDateOutput  = array();
    while($expiryRow = mysql_fetch_array($expiryResult))
    {
      $expiryDateValues[$i] = $expiryRow['expiryDate'];
      $expiryDateOutput[$i] = $expiryRow['expiryDate'];
      $i++;
    }
  /////////////////////////////////////////////
  /////////////////////////////////////////////
    $vendorQuery = "SELECT * FROM vendor ORDER BY vendor";
    $vendorResult = mysql_query($vendorQuery);
    
    $i = 0;
    $vendorSelected = '';
    $vendorValues  = array();
    $vendorOutput  = array();
    while($vendorRow = mysql_fetch_array($vendorResult))
    {
      if(isset($_POST['vendor']) && $_POST['vendor'] == $vendorRow['vendor'])
        $vendorSelected = $vendorRow['vendor'];
      $vendorValues[$i] = $vendorRow['vendor'];
      $vendorOutput[$i] = $vendorRow['vendor'];
      $i++;
    }
  /////////////////////////////////////////////
  /////////////////////////////////////////////
    $lastTradeInfoVar = '';
    if(isset($_POST['submitBtn']))
    {
      $clientInfoQuery  = "SELECT * FROM client
                                WHERE clientId = ".$_POST['clientId'];
      $clientInfoResult = mysql_query($clientInfoQuery);
      
      while($clientInfoRow = mysql_fetch_array($clientInfoResult))
      {
        $nameToDisplay = $clientInfoRow['firstName']." ".$clientInfoRow['middleName']." ".$clientInfoRow['lastName'];
      }
      
      if(isset($_POST['standing']) && $_POST['standing']==1)
        $standDisplay = " * Close Standing";
      elseif(isset($_POST['standing']) && $_POST['standing']==2)
        $standDisplay = " * Open Standing";
      else
        $standDisplay = "";
      if(isset($_POST['expiryDate']) && strlen($_POST['expiryDate']) > 0)
        $expiryDate = $_POST['expiryDate'];
      else
        $expiryDate = "";
//      $lastTradeInfoVar = $nameToDisplay." * Date : ".$_POST['tradeDay']."-".$_POST['tradeMonth']."-".$_POST['tradeYear']." * Price : ".$_POST['price']." * ".$_POST['itemId']." ".$expiryDate." * ".$_POST['buySell']." * Qty : ".$_POST['qty'].$standDisplay." * Vendor : ".$_POST['vendor'];
    }
    else
    {
      $lastTradeIdQuery  = "SELECT max(tradeId) AS maxTradeId FROM tradetxt";
      $lastTradeIdResult = mysql_query($lastTradeIdQuery);
      
      while($lastTradeIdRow = mysql_fetch_array($lastTradeIdResult))
      {
        if($lastTradeIdRow['maxTradeId'] > 0)
        {
          $lastTradeInfoQuery  = "SELECT * FROM tradetxt
                                    WHERE tradeId = ".$lastTradeIdRow['maxTradeId'];
          $lastTradeInfoResult = mysql_query($lastTradeInfoQuery);
          
          while($lastTradeInfoRow = mysql_fetch_array($lastTradeInfoResult))
          {
            $clientInfoQuery  = "SELECT * FROM client
                                      WHERE clientId = ".$lastTradeInfoRow['clientId'];
            $clientInfoResult = mysql_query($clientInfoQuery);
            
            while($clientInfoRow = mysql_fetch_array($clientInfoResult))
            {
              $nameToDisplay = $clientInfoRow['firstName']." ".$clientInfoRow['middleName']." ".$clientInfoRow['lastName'];
            }
            
            if($lastTradeInfoRow['standing']==1)
              $standDisplay = " * Close Standing";
            elseif($lastTradeInfoRow['standing']==2)
              $standDisplay = " * Open Standing";
            else
              $standDisplay = "";
    
//            $lastTradeInfoVar = $nameToDisplay." * Date : ".substr($lastTradeInfoRow['tradeDate'],8,2)."-".substr($lastTradeInfoRow['tradeDate'],5,2)."-".substr($lastTradeInfoRow['tradeDate'],2,2)." * Price : ".$lastTradeInfoRow['price']." * ".$lastTradeInfoRow['itemId']." ".$lastTradeInfoRow['expiryDate']." * ".$lastTradeInfoRow['buySell']." * Qty : ".$lastTradeInfoRow['qty'].$standDisplay." * Vendor : ".$lastTradeInfoRow['vendor'];
          }
        }
      }
    }
//////////////////////////////////////////////////
//////////////////////////////////////////////////
    if(isset($_POST['changedField']) && $_POST['changedField'] == "itemId")
      $focusScript = '<SCRIPT language="javascript">document.form1.itemId.focus();</SCRIPT>';
    elseif(isset($_POST['changedField']) && $_POST['changedField'] == "clientId")
      $focusScript = '<SCRIPT language="javascript">document.form1.clientId.focus();</SCRIPT>';
    else
      $focusScript = '<SCRIPT language="javascript">document.form1.itemId.focus();</SCRIPT>';
  //////////////////////////////////////////////////
    if(isset($_GET['forStand']))
      $forStand = $_GET['forStand'];
    elseif(isset($_POST['forStand']))
      $forStand = $_POST['forStand'];
    else
      $forStand = 0;
  //////////////////////////////////////////////////
  
///////
  if(isset($_POST['limitBtn']))
    $limitTrade = $_POST['limitBtn'];
  elseif(isset($_POST['tradeBtn']))
    $limitTrade = $_POST['tradeBtn'];
  else
    $limitTrade = 0;
    
  if (isset($_POST['makeTrade']) && $_POST['makeTrade'] == 1)
    $lastTradeInfoVar = $_POST['clientId']." * ".$limitTrade." * Price : ".$_POST['price']." * ".$_POST['itemId']." ".$expiryDate." * ".$_POST['buySell']." * Qty : ".$_POST['qty']." * Vendor : ".$_POST['vendor'];
  else
    $lastTradeInfoVar = "";
///////

   $selectItem="SELECT * FROM item ORDER BY itemId";
  
  $resultItem=mysql_query($selectItem);
  if(!$resultItem)
  {
    echo "No Item Selected";
  }
  else
  {
    $k=0;
    while($row=mysql_fetch_array($resultItem))
    {
      $itemId[$k]=$row['itemId'];
      $min[$k]=$row['min'];
      
      //SELECTION OF EXPIRYDATE:START
      
      $selectExpiry="SELECT * FROM expiry WHERE itemId='".$itemId[$k]."'";
      
      $resultExpiry=mysql_query($selectExpiry);
      if(!$resultExpiry)
      {
        echo "No Expiry Selected";
      }
      else
      {
        $l=0;
        while($row=mysql_fetch_array($resultExpiry))
        {
          $expiryDate[$k][$l]=$row['expiryDate'];
          $expiryDateSelected[$l]=$row['expiryDate'];
          $l++;
          $smarty->assign("expiryDate",$expiryDate);
          //$smarty->assign("expiryDateSelected",$expiryDateSelected);
        }
      }
      //SELECTION OF EXPIRYDATE:END
  
      $k++;
      $smarty->assign("itemId",$itemId);
      $smarty->assign("min",$min);
      $smarty->assign("l",$l);
      $smarty->assign("k",$k);
      
    }
  }
  //SELECTION OF ITEM:END
    
    $smarty->assign("orderTypeSelected",  $orderTypeSelected);
    $smarty->assign("orderTypeValues",    $orderTypeValues);
    $smarty->assign("orderTypeOutput",    $orderTypeOutput);
    $smarty->assign("itemFromPriceJS",  $itemFromPriceJS);
    $smarty->assign("PHP_SELF",         $_SERVER['PHP_SELF']);
    $smarty->assign("firstName",        $firstName);
    $smarty->assign("middleName",       $middleName);
    $smarty->assign("lastName",         $lastName);
    $smarty->assign("tradeDateDisplay", $tradeDateDisplay);
    $smarty->assign("buySellSelected",  $buySellSelected);
    $smarty->assign("buySellValues",    $buySellValues);
    $smarty->assign("buySellOutput",    $buySellOutput);
    $smarty->assign("itemIdSelected",   $itemIdSelected);
    $smarty->assign("itemIdValues",     $itemIdValues);
    $smarty->assign("itemIdOutput",     $itemIdOutput);
    $smarty->assign("expiryDateSelected", $expiryDateSelected);
    $smarty->assign("expiryDateValues"  , $expiryDateValues);
    $smarty->assign("expiryDateOutput"  , $expiryDateOutput);
    $smarty->assign("lastPrice"         , $lastPrice);
    $smarty->assign("vendorSelected"    , $vendorSelected);
    $smarty->assign("vendorValues"      , $vendorValues);
    $smarty->assign("vendorOutput"      , $vendorOutput);
    $smarty->assign("clientWholeName"   , $clientWholeName);
    $smarty->assign("minQty"            , $minQty);
    $smarty->assign("lastTradeInfoVar"  , $lastTradeInfoVar);
    $smarty->assign("focusScript"  ,      $focusScript);
    $smarty->assign("forStand"          , $forStand);
    $smarty->display("tradeEdit.tpl");
  }
  else
    echo "No clients added !";
?>