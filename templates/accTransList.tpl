<BODY>
<A href="./accTransOtherAdd.php">Add</A>&nbsp;&nbsp;
<A href="./mnuAccount.php">Menu</A><BR><BR>
<FORM name=Form1 action="{$PHP_SELF}" method=POST>
<TABLE border="0" cellPadding="2" cellSpacing="0">
<TR>
  <TD>
      Name : <SELECT name=cboName onChange="document.Form1.submit();">
            {section name=sec1 loop=$i}
            <OPTION value={$resuId[sec1]} {$SELECTED[sec1]}>
            {$resuName[sec1]}
            </OPTION>
            {/section}
            </SELECT>
  </TD>
 	<TD>&nbsp;&nbsp;Transaction In&nbsp;</TD>
 	<TD><select name="transModeOpt" onChange="document.Form1.submit();">
        <OPTION value='All' {$all}>All </OPTION>
        <OPTION value='Cash' {$cash}>Cash </OPTION>
        {section name=sec loop=$b}
        {html_options values=$bankId[sec] output=$bank[sec] selected=$selectBankId}
        <!--<OPTION value={$bankId[sec]} {$selectBankId}>
        {$bank[sec]}
        </OPTION>-->
        {/section}
    </select>
  </Td>
 	<TD>&nbsp;&nbsp;Transaction Type &nbsp;</TD>
 	<TD>
 		<select name="drCr" onChange="document.Form1.submit();">
      <OPTION value='All' {$all}>All </OPTION>
      {html_options options=$drCrValue selected=$selectedMod}
 		</select></Td>
  <TD vAlign="top">
    <A href="selectDtSession.php?goTo=accTransList">Date range</A> : {$fromDate} To : {$toDate}
  </TD>
</TR>
</TABLE>
</FORM>
  <TABLE BORDER=1 cellspacing="0" cellpadding="3">
  <TR>
    <TH colspan="6">&nbsp;</TH>
    <TH>&nbsp;</TH>
    <TH colspan="3" align="center">Total</TH>
  </TR>
  <TR>
    <TD ALIGN="center">Delete</TD>
    <TD ALIGN="center">
    <B>  Date</B>
    </TD>
    <TD align="center">
    <B>Transaction In</B>
    </TD>
    <TD align="center">
    <B>Transaction Type</B>
    </TD>
    <TD ALIGN="center">
    <B> Note </B>
    </TD>
    {if $selectedMod == 'd'}
      <TD ALIGN="center">
        <B>! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Credit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</B>
      </TD>
    {elseif $selectedMod == 'w'}
      <TD ALIGN="center" NOWRAP>
        <B>! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Debit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</B>
      </TD>
    {else}
      <TD ALIGN="center">
        <B>! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Credit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</B>
      </TD>
      <TD ALIGN="center" NOWRAP>
        <B>! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Debit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</B>
      </TD>
    {/if}
    <TD ALIGN="center" >
      <B>&nbsp;</B>
    </TD>
    <TD ALIGN="center">
      <B>Margin Balance</B>
    </TD>
    <TD><B>Total Balance</B></TD>
  </TR>
    {section name=sec2 loop=$k}
      <TR>
        <TD>
          {if $itemIdExpiryDate[sec2]!="Opening"}
          <A onclick="return confirm('Are you sure?');" 
          href="accCashFlowDelete.php?cashFlowId={$cashFlowId[sec2]}">Delete</A>
          {else}
          &nbsp;
          {/if}
        </TD>
        <TD NOWRAP>{$reportDate[sec2]}</TD>
        <TD NOWRAP>{$transMode[sec2]}</TD>
        <TD>{$transType[sec2]}</TD>
        <TD NOWRAP>{$itemIdExpiryDate[sec2]}</TD>
        {if $selectedMod == 'd'}
          <TD NOWRAP align="right">{$reportdwAmount1[sec2]}</TD>
        {elseif $selectedMod == 'w'}
          <TD NOWRAP align="right">{$reportdwAmount2[sec2]}</TD>
        {else}
          <TD NOWRAP align="right"><FONT color="blue">{$reportdwAmount1[sec2]}</FONT></TD>
          <TD NOWRAP align="right"><FONT color="red">{$reportdwAmount2[sec2]}</FONT></TD>
        {/if}
        <TD><B>&nbsp;</B></TD>
        <TD NOWRAP align="right"><B>{$totMargin[sec2]}</B></TD>
        {if $reportcurrentBal[sec2] < 0}
          <TD NOWRAP align="right"><B><FONT color="red">{$reportcurrentBal[sec2]}</FONT></B></TD>
        {else}
          <TD NOWRAP align="right"><B><FONT color="blue">{$reportcurrentBal[sec2]}</FONT></B></TD>
        {/if}
      </TR>
    {/section}
        
  </TABLE>
  <br>
   Margin as on date {$toDate} = <B>{$storeTotMargin}</B> 
  <br>
   ---------------------------------------------------------
  <br>
   Balance as on date {$toDate} (Margin + Other) = <B>{$totalBalOnDate}</B>
</BODY>