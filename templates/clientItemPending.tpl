<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
{/literal}
</STYLE>  
</HEAD>
<BODY>
  <A href="index.php">Home</A>&nbsp;&nbsp;<BR><BR>
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
  <TD>
    Group : 
    <SELECT name="groupId" onChange="document.form1.submit();">
    {html_options output=$groupNames selected=$groupIdSelected values=$groupIds}
    </SELECT>
    Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    </SELECT>
  </TD>
  <TD>Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    </SELECT>
  </TD>
  <TD>Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    </SELECT>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo=clientTrades">Date range</A> : {$fromDate} To : {$toDate}</CENTER>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
</TABLE>
<TABLE border="1" cellPadding="4" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>&nbsp;</TD>
  <TD>Deposit</TD>
  <TD align="center">Item</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Net</TD>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].dispGross != 0}
  {if $trades[sec1].totBuyQty != $trades[sec1].totSellQty}
    <TR>
      <TD>{$trades[sec1].clientId} : {$trades[sec1].clientName}</TD>
      <TD align="right">{$trades[sec1].clientDeposit}</TD>
      <TD align="center">{$trades[sec1].itemIdExpiry}</TD>
      <TD align="right">{$trades[sec1].totBuyQty}</TD>
      <TD align="right">{$trades[sec1].buyRash}</TD>
      <TD align="right">{$trades[sec1].totSellQty}</TD>
      <TD align="right">{$trades[sec1].sellRash}</TD>
      <TD align="right" NOWRAP>
        {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}
      </TD>
    </TR>
  {/if}
{/if}
{/section}
<TR><TD colspan="6">&nbsp;</TD></TR>
<TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD align="center">Item</TD>
  <TD>Buy</TD>
  <TD>Rash</TD>
  <TD>Sell</TD>
  <TD>Rash</TD>
  <TD align="center">Net</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD align="center" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty}
    </TD>
  <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
</TR>
{/section}
<TR>
  <TD>&nbsp;</TD>
  <TD align="right" NOWRAP> : Total : </TD>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty}
    </TD>
  <TD align="right">{$wholeBuyQty}</TD>
  <TD align="right">{$wholeBuyRash}</TD>
  <TD align="right">{$wholeSellQty}</TD>
  <TD align="right">{$wholeSellRash}</TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
