{literal}
<SCRIPT>
  function tot(a,b)
  {
    {/literal}
      {section name="sec" loop="$trades"}
        if( '{$trades[sec].itemIdExpiryUnderscore}' == a)
        {literal}{{/literal}
          if(((parseFloat(document.form1.{$trades[sec].itemId}HIDDENBUYQTY{$trades[sec].clientId}.value)) > (parseFloat(document.form1.{$trades[sec].itemId}HIDDENSELLQTY{$trades[sec].clientId}.value))))
	          {literal}{{/literal}
	          	var sellQty = (parseFloat(document.form1.{$trades[sec].itemId}HIDDENBUYQTY{$trades[sec].clientId}.value)) - (parseFloat(document.form1.{$trades[sec].itemId}HIDDENSELLQTY{$trades[sec].clientId}.value));
		          //alert(sellQty);
		          document.form1.{$trades[sec].itemId}SELLPRICE{$trades[sec].clientId}.value = ((parseFloat(document.form1.{$trades[sec].itemId}HIDDENSELLQTY{$trades[sec].clientId}.value) * parseFloat(document.form1.{$trades[sec].itemId}HIDDENSELLPRICE{$trades[sec].clientId}.value)) + (parseFloat(sellQty) * parseFloat(b))) / (parseFloat(document.form1.{$trades[sec].itemId}HIDDENBUYQTY{$trades[sec].clientId}.value));
		          document.form1.{$trades[sec].itemId}SELLQTY{$trades[sec].clientId}.value = (parseFloat(document.form1.{$trades[sec].itemId}HIDDENSELLQTY{$trades[sec].clientId}.value) + parseFloat(sellQty));
		          document.form1.{$trades[sec].itemId}NET{$trades[sec].clientId}.value = ((parseFloat(document.form1.{$trades[sec].itemId}BUYQTY{$trades[sec].clientId}.value) - parseFloat(document.form1.{$trades[sec].itemId}SELLQTY{$trades[sec].clientId}.value))) * parseFloat(b);
		          // NSA = OSA + (DIFF * NEWAMT)
		          var newSellAmt = parseFloat(document.form1.org{$trades[sec].itemId}SellAmt{$trades[sec].clientId}.value) + (parseFloat(sellQty) * parseFloat(b));
		          // NBA = OBA
		          var newBuyAmt = parseFloat(document.form1.org{$trades[sec].itemId}BuyAmt{$trades[sec].clientId}.value);
		          // PLTODISP = NSA - NBA
		          document.form1.pltodisp{$trades[sec].itemId}{$trades[sec].clientId}.value = newSellAmt - newBuyAmt;
	          {literal}}{/literal}
          else
            {literal}{{/literal}
              var buyQty = (parseFloat(document.form1.{$trades[sec].itemId}HIDDENSELLQTY{$trades[sec].clientId}.value)) - (parseFloat(document.form1.{$trades[sec].itemId}HIDDENBUYQTY{$trades[sec].clientId}.value));
              //alert(buyQty);
              document.form1.{$trades[sec].itemId}BUYPRICE{$trades[sec].clientId}.value = ((parseFloat(document.form1.{$trades[sec].itemId}HIDDENBUYQTY{$trades[sec].clientId}.value) * parseFloat(document.form1.{$trades[sec].itemId}HIDDENBUYPRICE{$trades[sec].clientId}.value)) + (parseFloat(buyQty) * parseFloat(b))) / (parseFloat(document.form1.{$trades[sec].itemId}HIDDENSELLQTY{$trades[sec].clientId}.value));
              document.form1.{$trades[sec].itemId}BUYQTY{$trades[sec].clientId}.value = (parseFloat(document.form1.{$trades[sec].itemId}HIDDENBUYQTY{$trades[sec].clientId}.value) + parseFloat(buyQty));
              document.form1.{$trades[sec].itemId}NET{$trades[sec].clientId}.value = ((parseFloat(document.form1.{$trades[sec].itemId}BUYQTY{$trades[sec].clientId}.value) - parseFloat(document.form1.{$trades[sec].itemId}SELLQTY{$trades[sec].clientId}.value))) * parseFloat(b);
              // NBA = OBA + (DIFF * NEWAMT)
              var newBuyAmt = parseFloat(document.form1.org{$trades[sec].itemId}BuyAmt{$trades[sec].clientId}.value) + (parseFloat(buyQty) * parseFloat(b));
              // NSA = OSA
              var newSellAmt = parseFloat(document.form1.org{$trades[sec].itemId}SellAmt{$trades[sec].clientId}.value);
		          // PLTODISP = NSA - NBA
		          document.form1.pltodisp{$trades[sec].itemId}{$trades[sec].clientId}.value = newSellAmt - newBuyAmt;
            {literal}}{/literal}
        {literal}}{/literal}
      {/section}
    {literal}
  }
</SCRIPT>
{/literal}

<HTML>
  <HEAD><TITLE>Om : Client Trades !!!</TITLE>
  <STYLE>
  {literal}
  td{font-weight: BOLD}
  {/literal}
  </STYLE>  
  </HEAD>
  <BODY>
    <A href="index.php">Home</A>
    <FORM name="form1" method="get" action="storeClientWiseGross.php">
    <INPUT type="hidden" name="display" value="{$display}">
    <INPUT type="hidden" name="itemIdChanged" value="0">
    
    <TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
	    <TR>
	      <TD>Client : 
	        <SELECT name="clientId" >
	        {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
	        </SELECT>
	      </TD>
	      <TD>Item : 
	        <SELECT name="itemId" >
	        {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
	        </SELECT>
	      </TD>
	      <TD>Expiry : 
	        <SELECT name="expiryDate" >
	        {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
	        </SELECT>
	      </TD>
	    </TR>
	    <TR>
	      <TD colspan="3" align="center">
	        <A href="selectDtSession.php?goTo=clientTrades">Date range</A> : {$fromDate} To : {$toDate}</CENTER>
	      </TD>
	    </TR>
	    <TR>
	      <TD colspan="3" align="center">
	        {$message}
	      </TD>
	    </TR>
    </TABLE>

    <TABLE cellPadding="2" cellSpacing="0">
      <TR>
      {section name=secExpire loop=$item_expiryMMMArray}
        <TD> <LABEL> {$item_expiryDDMMMArray[secExpire]} </LABEL> </TD>
        <TD> <INPUT type="text" size=10 name="{$item_expiryMMMArray[secExpire]}" onChange="tot(this.name,this.value);"> </TD>
        <TD> &nbsp; </TD>
        {if ($smarty.section.secExpire.rownum%4) == 0}       
          </TR><TR>
        {/if}
      {/section}
      <TD> <INPUT type="submit" value="Save..."> <INPUT type="hidden" name="date" value="{$toDate}"> </TD>
      </TR>
    </TABLE>
    
    <TABLE border="1" cellPadding="2" cellSpacing="0">
	    <TR>
	      <TD rowspan=2 align="center">&nbsp;</TD>
	      <TD rowspan=2 align="center">PLToDisp</TD>
	      <TD rowspan=2 align="center">Net</TD>
	      <TD colspan="2" align="center">Buy</TD>
	      <TD colspan="2" align="center">Sell</TD>
	      <TD colspan="6">&nbsp;</TD>
	    </TR>
	    <TR>
	      <TD align="center">Qty</TD>
	      <TD align="center">Price</TD>
	      <TD align="center">Qty</TD>
	      <TD align="center">Price</TD>
	      <TD colspan="2" align="center">Item</TD>
	      <TD align="center">ProfitLoss</TD>
	      <TD colspan="2" align="center">Brok</TD>
	      <TD>NetProfitLoss</TD>
	    </TR>
	    {section name="sec1" loop="$trades"}
	    {if $trades[sec1].dispGross != 0}
	      <TR>
	        <TD>{$trades[sec1].clientId} : {$trades[sec1].clientName}</TD>
	        <TD> <INPUT type="text" size=10 name="pltodisp{$trades[sec1].itemId}{$trades[sec1].clientId}" disabled > </TD>
	        <TD align="right" NOWRAP>
	          <INPUT type="text" size=10 name="{$trades[sec1].itemId}NET{$trades[sec1].clientId}" value="{math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}" disabled >
	        </TD>
	        <TD align="right">
	          <INPUT type="text" size=10 name="{$trades[sec1].itemId}BUYQTY{$trades[sec1].clientId}" value="{$trades[sec1].totBuyQty}" disabled >
            <INPUT type="hidden" size=10 name="{$trades[sec1].itemId}HIDDENBUYQTY{$trades[sec1].clientId}" value="{$trades[sec1].totBuyQty}" >
          </TD>
	        <TD align="right">
	          <INPUT type="text" size=10 name="{$trades[sec1].itemId}BUYPRICE{$trades[sec1].clientId}" value="{$trades[sec1].buyRash}" disabled>
	          <INPUT type="hidden" size=10 name="{$trades[sec1].itemId}HIDDENBUYPRICE{$trades[sec1].clientId}" value="{$trades[sec1].buyRash}">
	          <INPUT type="text" size=10 name="org{$trades[sec1].itemId}BuyAmt{$trades[sec1].clientId}" value="0" >
	       </TD>
	       <TD align="right">
	         <INPUT type="text" size=10 name="{$trades[sec1].itemId}SELLQTY{$trades[sec1].clientId}" value="{$trades[sec1].totSellQty}" disabled>
	         <INPUT type="hidden" size=10 name="{$trades[sec1].itemId}HIDDENSELLQTY{$trades[sec1].clientId}" value="{$trades[sec1].totSellQty}">
	       </TD>
	       <TD align="right">
	         <INPUT type="text" size=10 name="{$trades[sec1].itemId}SELLPRICE{$trades[sec1].clientId}" value="{$trades[sec1].sellRash}" disabled>
	         <INPUT type="hidden" size=10 name="{$trades[sec1].itemId}HIDDENSELLPRICE{$trades[sec1].clientId}" value="{$trades[sec1].sellRash}">
	         <INPUT type="text" size=10 name="org{$trades[sec1].itemId}SellAmt{$trades[sec1].clientId}" value="24000" >
	       </TD>
	       {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty} 
  	       <TD colspan="2">{$trades[sec1].itemIdExpiryUnderscore}</TD>
  	       <TD align="right" NOWRAP>{$trades[sec1].profitLoss}</TD>
  	       <TD colspan="2" align="right" NOWRAP>{$trades[sec1].oneSideBrok}</TD>
  	       <TD align="right" NOWRAP>{$trades[sec1].netProfitLoss}</TD>
	       {else}
	         <TD colspan="7">{$trades[sec1].itemIdExpiryUnderscore} : Buy Sell Qty Not Same</TD>
	       {/if}
	      </TR>
	      {if $trades[sec1].dispClientWhole != 0}
  	      <TR>
  	        <TD colspan="7" align="right">
  	          <U>{$trades[sec1].clientId} : {$trades[sec1].clientName}</U>
  	        </TD>
  	        <TD colspan="2" align="right"><U> : Total : </U>
  	        </TD>
  	        <TD align="right">
  	          <U>{$trades[sec1].clientTotProfitLoss}</U>
  	        </TD>
  	        <TD colspan="2" align="right">
  	          <U>{$trades[sec1].clientTotBrok}</U>
  	        </TD>
  	        <TD align="right"><U>{$trades[sec1].clientTotNetProfitLoss}</U></TD>
  	      </TR>
	      {/if}
	    {/if}
	    {/section}
	    <TR><TD colspan="12">&nbsp;</TD></TR>
	    <TR>
	      <TD>&nbsp;</TD>
	      <TD align="center">Net</TD>
	      <TD>Buy</TD>
	      <TD>Rash</TD>
	      <TD>Sell</TD>
	      <TD>Rash</TD>
	      <TD colspan="2" align="center">Item</TD>
	      <TD align="center">ProfitLoss</TD>
	      <TD colspan="2" align="center">Brok</TD>
	      <TD align="center">NetProfitLoss</TD>
	    </TR>
	    {section name="sec2" loop="$wholeItemArr"}
	    <TR>
	      <TD>&nbsp;</TD>
	      <TD align="right" NOWRAP>
	        {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty}
	      </TD>
	      <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
	      <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
	      <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
	      <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
	      <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiryUnderscore}</TD>
	      <TD align="right" NOWRAP>{$wholeItemArr[sec2].profitLoss}</TD>
	      <TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
	      <TD align="right" NOWRAP>{$wholeItemArr[sec2].netProfitLoss}</TD>
	    </TR>
	    {/section}
	    <TR>
	      <TD>&nbsp;</TD>
	      <TD align="right" NOWRAP>
	        {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty}
	      </TD>
	      <TD align="right">{$wholeBuyQty}</TD>
	      <TD align="right">{$wholeBuyRash}</TD>
	      <TD align="right">{$wholeSellQty}</TD>
	      <TD align="right">{$wholeSellRash}</TD>
	      <TD align="right" colspan="2" NOWRAP> : Total : </TD>
	      <TD align="right" NOWRAP>{$wholeProfitLoss}</TD>
	      <TD align="right" colspan="2">{$wholeOneSideBrok}</TD>
	      <TD align="right" NOWRAP>{$wholeNetProfitLoss}</TD>
	    </TR>
    </TABLE>
    </FORM>
  </BODY>
</HTML>
