<BODY>
<center>  
<A href="./mnuAccount.php">Menu</A><BR><BR>
<FORM name=Form1 action="{$PHP_SELF}" method=POST>

  <TABLE BORDER=1 cellspacing="0" cellpadding="3">
  <TR>
    <TD vAlign="top" colspan="4">
      <A href="selectDtSession.php?goTo=accSummary">Date range</A> : {$toDate}
    </TD>
  <TR>
    <TH align="center">Name</TH>
    <TH align="center">Credit</TH>
    <TH align="center">Debit</TH>
  </TR>
   {section name=sec1 loop=$clientArray}
  <TR>
    <TD align="center">
      {$clientArray[sec1].firstName} {$clientArray[sec1].middleName} {$clientArray[sec1].lastName} 
    </TD>
    <TD align="right">
      {if $clientArray[sec1].balance >= 0} {$clientArray[sec1].balance} 
      {else}                               &nbsp;
      {/if}
    </TD>
    <TD align="right">
      {if $clientArray[sec1].balance < 0} {$clientArray[sec1].balance} 
      {else}                               &nbsp;
      {/if}
    </TD>
  </TR>
    {/section}
   <TR>
    <TD Align="right" >
      <b>Total :     
    </TD>
    <TD Align="right">
      <b>{$creditTotal}</b>
    </TD>
    <TD Align="right">
      <b>{$debitTotal}</b>
    </TD>
   </TR>
   <TR>
    <TD Align="right" >
      <b>Total Balance :     
    </TD>
    <TD Align="center" colspan="2">
      <b>{$grandTotal}</b>
    </TD>
   </TR>
</TABLE>
   <!--Balance as on date {$toDate} = <B>{$totalBalOnDate}</B>-->
</BODY>
</center>