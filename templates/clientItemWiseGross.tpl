<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
{/literal}
</STYLE>  
</HEAD>
<BODY>
  <A href="index.php">Home</A>&nbsp;&nbsp;<BR><BR>
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
  <TD>Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    </SELECT>
  </TD>
  <TD>Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    </SELECT>
  </TD>
  <TD>Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    </SELECT>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo=clientTrades">Date range</A> : {$fromDate} To : {$toDate}</CENTER>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
</TABLE>
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="6">&nbsp;</TD>
</TR>
<TR>
  <TD>&nbsp;</TD>
  <TD align="center">Net</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD>NetProfitLoss</TD>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].dispGross != 0}
  <TR>
    <TD>{$trades[sec1].clientId} : {$trades[sec1].clientName}</TD>
    <TD align="right" NOWRAP>
      {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}
    </TD>
    <TD align="right">{$trades[sec1].totBuyQty}</TD>
    <TD align="right">{$trades[sec1].buyRash}</TD>
    <TD align="right">{$trades[sec1].totSellQty}</TD>
    <TD align="right">{$trades[sec1].sellRash}</TD>
  {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty} 
    <TD colspan="2">{$trades[sec1].itemIdExpiry}</TD>
    <TD align="right" NOWRAP>{$trades[sec1].profitLoss}</TD>
    <TD colspan="2" align="right" NOWRAP>{$trades[sec1].oneSideBrok}</TD>
    <TD align="right" NOWRAP>{$trades[sec1].netProfitLoss}
    </TD>
  {else}
    <TD colspan="6">{$trades[sec1].itemIdExpiry} : Buy Sell Qty Not Same</TD>
  {/if}
  </TR>
  {if $trades[sec1].dispClientWhole != 0}
  <TR>
    <TD colspan="6" align="right">
      <U>{$trades[sec1].clientId} : {$trades[sec1].clientName}</U>
    </TD>
    <TD colspan="2" align="right"><U> : Total : </U>
    </TD>
    <TD align="right">
      <U>{$trades[sec1].clientTotProfitLoss}</U>
    </TD>
    <TD colspan="2" align="right">
      <U>{$trades[sec1].clientTotBrok}</U>
    </TD>
    <TD align="right"><U>{$trades[sec1].clientTotNetProfitLoss}</U></TD>
  </TR>
  {/if}
{/if}
{/section}
<TR><TD colspan="12">&nbsp;</TD></TR>
<TR>
  <TD>&nbsp;</TD>
  <TD align="center">Net</TD>
  <TD>Buy</TD>
  <TD>Rash</TD>
  <TD>Sell</TD>
  <TD>Rash</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD align="center">NetProfitLoss</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
  <TD>&nbsp;</TD>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty}
    </TD>
  <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
  <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
  <TD align="right" NOWRAP>{$wholeItemArr[sec2].profitLoss}</TD>
  <TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
  <TD align="right" NOWRAP>{$wholeItemArr[sec2].netProfitLoss}</TD>
</TR>
{/section}
<TR>
  <TD>&nbsp;</TD>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty}
    </TD>
  <TD align="right">{$wholeBuyQty}</TD>
  <TD align="right">{$wholeBuyRash}</TD>
  <TD align="right">{$wholeSellQty}</TD>
  <TD align="right">{$wholeSellRash}</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" NOWRAP>{$wholeProfitLoss}</TD>
  <TD align="right" colspan="2">{$wholeOneSideBrok}</TD>
  <TD align="right" NOWRAP>{$wholeNetProfitLoss}</TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
