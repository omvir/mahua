<LINK rel="stylesheet" type="text/css" href="./menuFiles/ddsmoothmenu.css" />
<SCRIPT type="text/javascript" src="./menuFiles/jquery.min.js"></SCRIPT>
<SCRIPT type="text/javascript" src="./menuFiles/ddsmoothmenu.js"></SCRIPT>
<DIV id="smoothmenu1" class="ddsmoothmenu">
<UL>

<LI><A href="#">Masters</A></LI>
  <UL>
    <LI><A href="brokerage.php">Items</A></LI>
    <LI><A href="expiryList.php">Expiry Dates</A></LI>
    <LI><A href="clientList.php">Clients and Brokers</A></LI>
    <LI><A href="bankMasterList.php">Bank Masters</A></LI>
    <LI><A href="otherExpAdd.php">Other Expense</A></LI>
    <LI><A href="exchangeAdd.php">Exchange</A></LI>
  </UL>
</LI>

<LI><A href="#">Trades</A>
  <UL>
    <LI><A href="#">Mcx Trades</A>
    	<UL>
    	  <LI><A href="clientTradesMcx.php"                 >Client Mcx Trades</A></LI>
		    <LI><A href="brokerTradesMcx.php"                 >Broker Mcx Trades</A></LI>
    	  <LI><A href="clientTradesMcx.php?display=detailed">Client Detailed Trades</A></LI>
		    <LI><A href="clientTradesMcx.php?display=detailed">Broker Detailed Trades</A></LI>
    	</UL>
    </LI>
    <LI><A href="selectDtSession.php?goTo=clientTrades">Detailed Trades</A></LI>
    <LI><A href="txtFile.php">Store TWS Trades</A></LI>
    <LI><A href="txtFileOdin.php">Store ODIN Trades</A></LI>
    <LI><A href="ajxStoreLimitTradeMcx1.php">Data Entry Mcx Fast</A></LI>
    <LI><A href="#">&nbsp;</A></LI>
    <LI><A href="ajxStoreLimitTradeMcx1.php?useTpl=txtbx">Data Entry Mcx ClientId</A></LI>
  </UL>
</LI>

<LI><A href="#">Other Options</A>
  <UL>
    <LI><A href="tradeListForCrossChecking.php">Confirm</A></LI>
    <LI><A href="orderList.php">Order List</A></LI>
    <LI><A href="attachNameToTxtData.php">Pending Trades</A></LI>
    <LI><A href="attachNameToTxtDataAll.php">Change Name</A></LI>
    <LI><A href="deleteAll.php">Delete Trades</A></LI>
  </UL>
</LI>

<LI><A href="#">Settlement</A>
  <UL>
    <LI><A href="mnuStand.php"                                   >Standing</A></LI>
    <LI><A href="clientTradesMcx.php?display=gross"              >Generate Bill MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=tradesPrint"        >Bill Print MCX</A></LI>
    <LI><A href="#">Rollback</A></LI>
  </UL>
</LI>

<LI><A href="#">Reports</A>
  <UL>
    <LI><A href="clientTradesMcx.php?display=itemPending">Script Outstanding MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemPending2">Average Outstanding MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemWiseGross">Script Gross MCX</A></LI>
    <LI><A href="clientTradesMcx.php?display=gross">Bill Gross MCX Client</A></LI>
    <LI><A href="brokerTradesMcx.php?display=gross">Bill Gross MCX Broker</A></LI>
  </UL>
</LI>

<LI><A href="#">Accounts</A>
  <UL>
    <LI><A href="accSummary.php">Account Last Balance</A></LI>
    <LI><A href="mnuAccount.php">Money Transaction</A></LI>
  </UL>
</LI>

<LI><A href="#">Utilities</A>
  <UL>
    <LI><A href="writeTradesFile.php">Online Backup</A></LI>
    <LI><A href="backup.php">Backup</A></LI>
    <LI><A href="links.php">Useful Links</A></LI>
    <LI><A href="calc.php">Calculator</A></LI>
    <LI><A href="changePassword.php">Change Password</A></LI>
    <LI><A href="#">ExtraMenu</A>
      <UL>
        <LI><A href="storeLimitTradeMcx.php">Data Entry Mcx Slow</A></LI>
      </UL>
    </LI>
  </UL>
</LI>

<LI><A href="selectDtSession.php">Date Range</A>
</LI>
<LI><A href="logout.php">Logout</A>
</LI>

</UL>
<BR style="clear: left" />
</DIV>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>