<HTML>
<HEAD><TITLE>Om !!! Add Trade</TITLE>
  {literal}
<STYLE type="text/css">
.body
{
  color:White;
}
</STYLE>


<SCRIPT language="javascript">
function blankCheck()
{
  if(!checkDate())
    return false;
  if(document.form1.clientId.value == '')
  {
    alert('Client Id could not be Blank');
    document.form1.clientId.value='';
    document.form1.clientId.focus();
    return false;
  }
  else if(document.form1.clientId2.value == '')
  {
    alert('Client Id-2 could not be Blank');
    document.form1.clientId2.value='';
    document.form1.clientId2.focus();
    return false;
  }
  else if(document.form1.price.value == '')
  {
    alert('Price could not be Blank');
    document.form1.price.value='';
    document.form1.price.focus();
    return false;
  }
  else if(document.form1.expiryDate.value == '')
  {
    alert('Expiry Date could not be Blank');
    document.form1.itemId.focus();
    return false;
  }
  else if(document.form1.qtyLot.value == '')
  {
    alert('Quantity Lot could not be Blank');
    document.form1.qtyLot.focus();
    return false;
  }
  else
  {
    saveRecord();
    return true;
  }
} 
  
window.name = 'displayAll';
var minQtytoUse = 0;
function change()
 {
  var select1value= document.form1.itemId.value;
  {/literal}
   {section name=sec1 loop=$k}
    if( select1value=="{$itemId[sec1]}")
  	{literal}{{/literal}
    {literal}
      document.form1.qty.value={/literal}{$min[sec1]}{literal}
      minQtytoUse = document.form1.qty.value;
    }
      {/literal}
  {/section}
 }
{literal}

function changePrice(thePriceObject)
{
  var price;
  price = parseFloat(thePriceObject.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(thePriceObject.value != price)
      thePriceObject.value = price;
  }
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
{/literal}
{$itemFromPriceJS}
{literal}
}
function changeQtyLot()
{
  {/literal}
  var mQty = minQtytoUse;
  {literal}
  var qLot;
  qLot = parseFloat(document.form1.qtyLot.value);
  var lotQty;
  lotQty = qLot * mQty;
    document.form1.qty.value=lotQty; 
}
function qtyLotChange(theQtyLotObject)
{
  var qtyLt;
  qtyLt = parseFloat(theQtyLotObject.value);
  if(parseFloat(qtyLt) || qtyLt==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      qtyLt=qtyLt+1;
    if(event.keyCode==33)
      qtyLt=qtyLt+5;
    if(event.keyCode==40)
      qtyLt=qtyLt-1;
    if(event.keyCode==34)
      qtyLt=qtyLt-5;
    if(theQtyLotObject.value != qtyLt)
       theQtyLotObject.value = qtyLt;
  }
  changeQtyLot();
}

function changeQty()
{
  {/literal}
  var minQty = {$minQty};
  {literal}
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      qty=qty+minQty;
    if(event.keyCode==33)
      qty=qty+minQty*5;
    if(event.keyCode==40)
      qty=qty-minQty;
    if(event.keyCode==34)
      qty=qty-minQty*5;
    if(document.form1.qty.value != qty)
      document.form1.qty.value = qty;
  }
}
function changeBuySale()
{
  if(event.keyCode == 107 || event.keyCode==120)
  {
    document.form1.buySell.value = "Buy";
    return false;
  }
  if(event.keyCode == 109 || event.keyCode==123)
  {
    document.form1.buySell.value = "Sell";
    return false;
  }
  return true;
}
function checkBuySell()
{
  if(document.form1.buySell.value == "Buy" || document.form1.buySell.value == "Sell" )
  {}
  else
    document.form1.buySell.value = document.form1.beforeBuySell.value;
}

function bodyKeyPress()
{  
  if(event.keyCode==107 || event.keyCode==120)
  {
    window.document.bgColor="blue";
  }
  else if(event.keyCode==109 || event.keyCode==123)
  {
    window.document.bgColor="red";
  }
  
  if(event.keyCode==117)
  {
    orderListWindow = window.open('orderList.php?listOnly=1', 'orderListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode==119)
  {
    tradeListWindow = window.open('clientTradesMcx.php', 'tradeListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode == 13) 
  {
    if(event.ctrlKey && event.shiftKey)
    {
      if(confirm("Save As SL?"))
      {
        document.form1.makeTrade.value=3;
        blankCheck();
        return true;
      }
    }
    else if(event.ctrlKey)
    {
      if(confirm("Save As Trade?"))
      {
        document.form1.makeTrade.value=1;
        blankCheck();
        return true;
      }
    }
    else if(event.shiftKey)
    {
      if(confirm("Save As RL?"))
      {
        document.form1.makeTrade.value=2;
        blankCheck();
        return true;
      }
    }
    else
    {
    	if(window.event.srcElement.name == "submitBtnRl" || window.event.srcElement.name == "submitBtnSl" 
    	         || window.event.srcElement.name == "submitBtn")
        document.form1.clientId.focus();
    	else
    	  window.event.keyCode = 9;
    }	
  }
  return changeBuySale();
}

function checkButton(a)
{
	if(a =='submitBtnRl')
	{
	  if(confirm('Save As RL?'))
	  {
  		document.form1.makeTrade.value=2;
  		blankCheck();
	  	return true;
	  }
	}
	else if(a == 'submitBtnSl')
	{
	  if(confirm('Save As SL?'))
	  {
  		document.form1.makeTrade.value=3;
  		blankCheck();
	  	return true;
	  }
	}
	else
	{
	  if(confirm('Save As Trade?'))
	  {
  		document.form1.makeTrade.value=1;
  		blankCheck();
	  	return true;
	  }
	}
}

function askConfirmOk()
{
  if(!document.form1.expiryDate)
  {
    alert("Please Select Item");
    document.form1.itemId.focus();
  }
  else
    return false;
}

function getClientName(clientNm,clientCode)
{
  var sIdClientNm = clientNm;
  var sIdClientCode = clientCode;
  var url="./ajxDispClientName.php";
  var params = "clientNm="+escape(sIdClientNm)
                          +"&clientCode="+escape(sIdClientCode);
  http2.open("POST",url,true);
  http2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http2.setRequestHeader("Content-length", params.length);
	http2.setRequestHeader("Connection", "close");
	http2.send(params);
 	http2.onreadystatechange = handleHttpResponse2;
}
function getClientName1(clientNm,clientCode)
{
  var sIdClientNm = clientNm;
  var sIdClientCode = clientCode;
  var url="./ajxDispClientName.php";
  var params = "clientNm="+escape(sIdClientNm)
                          +"&clientCode="+escape(sIdClientCode);
  http3.open("POST",url,true);
  http3.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http3.setRequestHeader("Content-length", params.length);
	http3.setRequestHeader("Connection", "close");
	http3.send(params);
 	http3.onreadystatechange = handleHttpResponse3;
}

function changeItem(str)
{
  //xmlHttp1=getHTTPObject();

  var url="ajxExpiryCombo.php";
  url=url+"?q="+str;
  //url=url+"&sid="+Math.random();
  //xmlHttp1.onreadystatechange=stateChanged;
  http1.open("GET",url,true);
  http1.onreadystatechange = handleHttpResponse1;
  http1.send(null);
  
  change();
}
function saveRecord()
{
  var sIdmakeTrade = document.getElementById("makeTrade").value;
  var sIdday = document.getElementById("fromDateDay").value;
  var sIdmonth = document.getElementById("fromDateMonth").value;
  var sIdyear = document.getElementById("fromDateYear").value;
  var sIdclientId = document.getElementById("clientId").value;
  var sIdprice = document.getElementById("price").value;
  var sIdprice2 = document.getElementById("price2").value;
  var sIditemId = document.getElementById("itemId").value;
  var sIdexpiryDate = document.getElementById("expiryDate").value;
  var sIdbuySell = document.getElementById("buySell").value;
  var sIdqty = document.getElementById("qty").value;
  var sIdclientId2 = document.getElementById("clientId2").value;
  var sIdvendor = document.getElementById("vendor").value;
  var sIdStanding = document.getElementById("standing").value;
  
  var now = new Date();
  var hour = now.getHours();
  if (hour < 10)
    hour = '0'+hour;
  var minute = now.getMinutes();
  if (minute < 10)
    minute = '0'+minute;
  var second = now.getSeconds();
  if (second < 10)
    second = '0'+second;
  var sTime = hour + ":" + minute + ":" + second;
	var url = "ajxStoreLimitTradeMcx1.php";
	var params = "makeTrade="+escape(sIdmakeTrade)
	            +"&tradeDay="+escape(sIdday)
              +"&tradeMonth="+escape(sIdmonth)
              +"&tradeYear="+escape(sIdyear)
              +"&clientId="+escape(sIdclientId)
              +"&price="+escape(sIdprice)
              +"&price2="+escape(sIdprice2)
              +"&itemId="+escape(sIditemId)
              +"&expiryDate="+escape(sIdexpiryDate)
              +"&buySell="+escape(sIdbuySell)
              +"&qty="+escape(sIdqty)
              +"&clientId2="+escape(sIdclientId2)
              +"&vendor="+escape(sIdvendor)
              +"&standing="+escape(sIdStanding)
              +"&sTime="+escape(sTime);
  http.open("POST",url,true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.setRequestHeader("Content-length", params.length);
	http.setRequestHeader("Connection", "close");
	http.send(params);
	http.onreadystatechange = handleHttpResponse;
	//var sIdprice = document.getElementById("price").value = "";
	document.form1.clientId.focus();
}

	// End Using Post Method.
		function getHTTPObject()
		{
		  var xmlhttp;
			  if(window.XMLHttpRequest)
			  {	xmlhttp = new XMLHttpRequest(); }
  			  else if (window.ActiveXObject)
			  {		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    		   		if (!xmlhttp) {
        				xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); }
			  }
   		  return xmlhttp;
		}
	{/literal}
	var http = getHTTPObject(); // We create the HTTP Object

  {literal}
	function handleHttpResponse()
	{
		if (http.readyState == 4 || http.readyState == 'complete')
		{
			if(http.status==200)
			{
				var results=http.responseText;
			}
		}
	}
{/literal}
var http1 = getHTTPObject();
{literal}
	function handleHttpResponse1()
	{
		if (http1.readyState == 4 || http1.readyState == 'complete')
		{
			if(http1.status==200)
			{
				var results1=http1.responseText;
				document.getElementById('expiryDate1').innerHTML = results1;
			}
		}
	}

{/literal}
var http2 = getHTTPObject();
{literal}
	function handleHttpResponse2()
	{
		if (http2.readyState == 4 || http2.readyState == 'complete')
		{
			if(http2.status==200)
			{
				var results2=http2.responseText;
				//alert(results2);
				document.getElementById('clientName1').innerHTML = results2;
			}
		}
	}
	
{/literal}

var http3 = getHTTPObject();
{literal}
	function handleHttpResponse3()
	{
		if (http3.readyState == 4 || http3.readyState == 'complete')
		{
			if(http3.status==200)
			{
				var results3=http3.responseText;
				//alert(results2);
				document.getElementById('clientName2').innerHTML = results3;
			}
		}
	}
{/literal}
</SCRIPT>
</HEAD>
{literal}
<BODY bgColor="blue" onLoad="change(); todayFun();" onKeyDown="return bodyKeyPress(); " class="body">
{/literal}
  <A href="./index.php"><FONT color="white">Home</FONT></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  [ Client-1 = <u><b><font style="color:yellow"><LABEL id="clientName1"></LABEL></font></b></u>&nbsp;&nbsp;]&nbsp;&nbsp;
  [ Client-2 = <u><b><font style="color:yellow"><LABEL id="clientName2"></LABEL></font></b></u>&nbsp;&nbsp;]
  <FORM name="form1" action="{$PHP_SELF}" METHOD="post">
  <INPUT type="hidden" name="changedField" >
  <INPUT type="hidden" name="makeTrade" value="0">
  <INPUT type="hidden" name="forStand" value="{$forStand}">
  <TABLE BORDER=1 cellPadding="2" cellSpacing="0">
  <TR>
    <TD>Date : 
    {html_select_date prefix="fromDate" start_year=-5 end_year=+2 day_value_format="%02d" month_value_format="%m" month_format="%b" field_order="DMY"}
    </TD>
    <TD>
    	Client 1 : <INPUT type="text" name="clientId" size=4 onkeyup="getClientName(this.name,this.value);">
      Client 2 : <INPUT type="text" name="clientId2" size=4 onkeyup="getClientName1(this.name,this.value);">
    </TD>
    <TD colspan="2" NOWRAP>
      Price : <INPUT size="10" type="text" name="price" value="{$lastPrice}"  onKeydown="changePrice(this);" onBlur="document.form1.price2.value=this.value; itemFromPrice(); changeItem(document.form1.itemId.value); qtyLotChange(this);">&nbsp;&nbsp;&nbsp;
    </TD>
    <TD>
      Price2 : <INPUT size="10" type="text" name="price2" value="{$lastPrice2}" onKeydown="changePrice(this);">&nbsp;&nbsp;&nbsp;
    </TD>
  </TR>
  <TR>
    <TD>
      <INPUT type="hidden" name="beforeBuySell" value="Buy">
      <INPUT type="text" name="buySell" value="Buy" size="8"
        onFocus="document.form1.beforeBuySell.value=document.form1.buySell.value;"
        onChange="checkBuySell();" DISABLED >
        <SELECT name="itemId" onChange="changeItem(this.value); changeQtyLot();">
          {html_options values="$itemIdValues" output="$itemIdOutput"}
      </SELECT>
   </TD>
   <TD>
      <div id="expiryDate1"> </div>
   </TD>
   <TD colspan="2" NOWRAP>
    Qty.by Lot : <INPUT type="text" name="qtyLot" size=8 value="1" onKeydown="qtyLotChange(this);" onchange="qtyLotChange(this);">&nbsp;&nbsp;
   </TD>
   <TD>
    Quantity : <INPUT type="text" name="qty" value="{$minQty}" onKeyDown="changeQty();" size=8  DISABLED>&nbsp;&nbsp;&nbsp;
    </TD>
  </TR>
  <TR>
{if $forStand == 1}
    <TD>
      <SELECT name="standing">
        <OPTION name="open" value="-1">Open Standing</OPTION>
        <OPTION name="close" value="1">Close Standing</OPTION>
      </SELECT>
    </TD>
{else}
  <INPUT type="hidden" name="standing" value="0">
{/if}
    <TD colspan="6">
      <INPUT type="hidden" name="vendor" value="_SELF">
    </TD>
  </TR>
  <TR>
    <TD colspan="6">
      <INPUT type="button" name="submitBtnRl" value="RL" onClick="checkButton(this.name);">
      <INPUT type="button" name="submitBtnSl" value="SL" onClick="checkButton(this.name);">
      <INPUT type="button" name="submitBtn" value="Trade" onClick="checkButton(this.name);">
       &nbsp;&nbsp;&nbsp;&nbsp;<B>Shift + Enter => RL ## Ctrl + Shift + Enter => SL ## Ctrl + Enter => Trade

    </TD>
  </TR>
  </TABLE>
  </FORM>
  
<SCRIPT language="javascript">
	{literal}
	function todayFun()
	{
		var today = new Date();
		var curDate = today.getDate();
		var curMonth = today.getMonth();
		var curYear = today.getYear();
		if(curDate < 10)
		  curDate = '0'+curDate;
		if(curMonth < 10)
		  curMonth = '0'+(parseInt(curMonth+1));
    document.form1.fromDateDay.value   = curDate;
    document.form1.fromDateMonth.value = curMonth;
    document.form1.fromDateYear.value  = curYear;
	}
	function checkDate()
	{
    var dateSubmited = document.form1.fromDateDay.value+'-'+document.form1.fromDateMonth.value+'-'+document.form1.fromDateYear.value;
    if(document.form1.fromDateMonth.value == 01 || document.form1.fromDateMonth.value == 03 || 
       document.form1.fromDateMonth.value == 05 || document.form1.fromDateMonth.value == 07 || 
       document.form1.fromDateMonth.value == 08 || document.form1.fromDateMonth.value == 10 || 
       document.form1.fromDateMonth.value == 12)
      return true;
    if(document.form1.fromDateMonth.value == 02)
    {
      if(document.form1.fromDateYear.value % 4 == 0)
      {
        if(document.form1.fromDateDay.value > '29')
        {
        	alert("Please enter valid date");
        	document.form1.fromDateDay.focus();
        	return false;
        }
        else
        {
         	return true;
        }
      }
      else if(document.form1.fromDateYear.value % 400 == 0)
      {
        if(document.form1.fromDateDay.value > '29')
        {
        	alert("Please enter valid date");
        	document.form1.fromDateDay.focus();
        	return false;
        }
        else
        {
        	return true;
        }
      }
      else if(document.form1.fromDateDay.value > '28')
      {
      	alert("Please enter valid date");
       	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
    if(document.form1.fromDateMonth.value == 04)
    {
      if(document.form1.fromDateDay.value > '30')
      {
      	alert("Please enter valid date");
      	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
    if(document.form1.fromDateMonth.value == 06)
    {
      if(document.form1.fromDateDay.value > '30')
    	{
      	alert("Please enter valid date");
       	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
    if(document.form1.fromDateMonth.value == 09)
    {
      if(document.form1.fromDateDay.value > '30')
      {
      	alert("Please enter valid date");
       	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
    if(document.form1.fromDateMonth.value == 11)
    {
      if(document.form1.fromDateDay.value > '30')
      {
      	alert("Please enter valid date");
       	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
  }
  {/literal}
  document.form1.clientId.focus();
</SCRIPT>
</BODY>
</HTML>