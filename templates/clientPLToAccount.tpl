<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
{/literal}
</STYLE>  
</HEAD>
<BODY bgcolor="TAN">
<FORM name="profitLossForm" action="storeProfitLoss.php" method="post">
<INPUT type="hidden" name="transactionDate" value="{$profitLossDate}">
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD colspan="8">Click 'Store' Button To Store Profit Loss To Account As On : {$toDate}</TD>
</TR>
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD>NetProfitLoss</TD>
  <TD><INPUT type="submit" name="profitLossStoreButton" value="Store!"></TD>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].dispGross != 0}
  <TR>
    <TD>{$trades[sec1].clientId} : {$trades[sec1].clientName}</TD>
  {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty} 
    <TD colspan="2" align="center">{$trades[sec1].itemIdExpiry}</TD>
    <TD align="right" NOWRAP>{$trades[sec1].profitLoss}</TD>
    <TD colspan="2" align="right" NOWRAP>{$trades[sec1].oneSideBrok}</TD>
    <TD align="right" NOWRAP>{$trades[sec1].netProfitLoss}
    </TD>
    <TD align="center">
      <INPUT type="checkbox" 
        name="profitLoss[{$trades[sec1].clientId}][{$trades[sec1].itemIdExpiry}]"
        value="{$trades[sec1].netProfitLossNotFormatted}">
    </TD>
  {else}
    <TD colspan="6">{$trades[sec1].itemIdExpiry} : Buy Sell Qty Not Same</TD>
    <TD>&nbsp;</TD>
  {/if}
  </TR>
{/if}
{/section}
<TR><TD colspan="8">&nbsp;</TD></TR>
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD align="center">NetProfitLoss</TD>
  <TD>&nbsp;</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
  <TD>&nbsp;</TD>
  <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
  <TD align="right" NOWRAP>{$wholeItemArr[sec2].profitLoss}</TD>
  <TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
  <TD align="right" NOWRAP>{$wholeItemArr[sec2].netProfitLoss}</TD>
  <TD>&nbsp;</TD>
</TR>
{/section}
<TR>
  <TD>&nbsp;</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" NOWRAP>{$wholeProfitLoss}</TD>
  <TD align="right" colspan="2">{$wholeOneSideBrok}</TD>
  <TD align="right" NOWRAP>{$wholeNetProfitLoss}</TD>
  <TD>&nbsp;</TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
