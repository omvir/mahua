<?php
include "./etc/om_config.inc";
session_start();
if(!isset($_SESSION['user'])
   || (isset($_SESSION['userType']) && isset($filesArr['accSummary'][$_SESSION['userType']]) 
        && $filesArr['accSummary'][$_SESSION['userType']] == 0))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:index.php");
}
else
{
  if(!isset($_SESSION['toDate'])) 
  {
    header("Location: selectDtSession.php?goTo=accSummary");
  }
  else
  {
    $smarty=new SmartyWWW();
    
    $clientQuery = "SELECT * FROM client
                    ORDER BY firstName,middleName,lastName";
    $clientResult = mysql_query($clientQuery);
    
    $creditTotal = 0;
    $debitTotal  = 0;
    $grandTotal  = 0;
    $i = 0;
    while($clientRow = mysql_fetch_array($clientResult))
    {
      $clientArray[$i]['firstName']  = $clientRow['firstName'];
      $clientArray[$i]['middleName'] = $clientRow['middleName'];
      $clientArray[$i]['lastName']   = $clientRow['lastName'];
      $clientArray[$i]['balance']    = 0;
      
      $cashFlowQuery = "SELECT * FROM cashflow
                        WHERE clientId = ".$clientRow['clientId']."
                          AND transactionDate <= '".$_SESSION['toDate']."'";
      $cashFlowResult = mysql_query($cashFlowQuery);
      while($cashFlowRow = mysql_fetch_array($cashFlowResult))
      {
        if($cashFlowRow['dwStatus'] != 'd')
          $clientArray[$i]['balance'] -= $cashFlowRow['dwAmount'];
        else
          $clientArray[$i]['balance'] += $cashFlowRow['dwAmount'];
        if($cashFlowRow['plStatus'] != 'p')
          $clientArray[$i]['balance'] += $cashFlowRow['plAmount'];
        else
          $clientArray[$i]['balance'] -= $cashFlowRow['plAmount'];
      }
      if($clientArray[$i]['balance'] >= 0)
        $creditTotal                += $clientArray[$i]['balance'];
      else
        $debitTotal                 += $clientArray[$i]['balance'];
      $i++;
    }
    $grandTotal = $creditTotal + $debitTotal;
  
    $smarty->assign("toDate",$_SESSION['toDate']);
    $smarty->assign("clientArray",$clientArray);
    $smarty->assign("creditTotal",$creditTotal);
    $smarty->assign("debitTotal",$debitTotal);
    $smarty->assign("grandTotal",$grandTotal);
    $smarty->display("accSummary.tpl");
  }
}
?>