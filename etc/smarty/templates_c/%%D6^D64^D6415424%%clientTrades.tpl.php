<?php /* Smarty version 2.6.10, created on 2016-05-10 14:25:45
         compiled from clientTrades.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'clientTrades.tpl', 22, false),array('function', 'math', 'clientTrades.tpl', 125, false),)), $this); ?>
<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
<?php echo '
td{font-weight: BOLD}
.lossStyle   {color: red}
.profitStyle {color: blue}
'; ?>

</STYLE>  
</HEAD>
<BODY onKeyPress="if(event.keyCode==27)  window.close();">
  <A href="index.php">Home</A>&nbsp;&nbsp;
<FORM name="form1" method="get" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
">
<INPUT type="hidden" name="display" value="<?php echo $this->_tpl_vars['display']; ?>
">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
<?php if ($this->_tpl_vars['userType'] != 'client'): ?>
  <TD>
    Group : 
    <SELECT name="groupId" onChange="document.form1.submit();">
    <?php echo smarty_function_html_options(array('output' => $this->_tpl_vars['groupNames'],'selected' => $this->_tpl_vars['groupIdSelected'],'values' => $this->_tpl_vars['groupIds']), $this);?>

    </SELECT>
    Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['clientIdSelected']),'values' => ($this->_tpl_vars['clientIdValues']),'output' => ($this->_tpl_vars['clientIdOptions'])), $this);?>

    </SELECT>
  </TD>
<?php endif; ?>
  <TD>Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['itemIdSelected']),'values' => ($this->_tpl_vars['itemIdValues']),'output' => ($this->_tpl_vars['itemIdOptions'])), $this);?>

    </SELECT>
  </TD>
  <TD>Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['expiryDateSelected']),'values' => ($this->_tpl_vars['expiryDateValues']),'output' => ($this->_tpl_vars['expiryDateOptions'])), $this);?>

    </SELECT>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo=<?php echo $this->_tpl_vars['goTo']; ?>
">Date range</A> : <?php echo $this->_tpl_vars['fromDate']; ?>
 To : <?php echo $this->_tpl_vars['toDate']; ?>
</CENTER>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <?php echo $this->_tpl_vars['message']; ?>

  </TD>
</TR>
</FORM>
</TABLE>
<FORM name="form2" method="post" action="./acStorePl.php">
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="6">&nbsp;</TD>
</TR>
<TR>
  <TD align="center">BuySell</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Date</TD>
  <TD align="center">Stand</TD>
  <TD align="center">Item</TD>
  <TD align="center">NetProfitLoss</TD>
<?php if ($this->_tpl_vars['userType'] != 'client'): ?>
  <TD align="center">Vendor</TD>
  <TD align="center">Delete</TD>
<?php endif; ?>
  <?php if ($this->_tpl_vars['display'] == 'detailed'): ?> 
    <TD align="center">UserRemarks</TD>
    <TD align="center">OwnClient</TD>
    <TD align="center" NOWRAP>TradeRefNo</TD>
  <?php endif; ?>
</TR>
<?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=($this->_tpl_vars['trades'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
<?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId'] != $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['prevClientId'] || $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemId'] != $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['prevItemId'] || $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['expiryDate'] != $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['prevExpiryDate']): ?>
  <TR>
    <TD colspan="5"><U><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
 : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientName']; ?>
</U>&nbsp;:&nbsp;(<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientDeposit']; ?>
)</TD>
    <TD colspan="6" align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
</TD>
  </TR>
<?php endif; ?>
<TR style="color:<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['fontColor']; ?>
">
  <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buySell']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyQty']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['price']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellQty']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellPrice']; ?>
</TD>
  <TD align="center" NOWRAP><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeDate']; ?>

    <?php if ($this->_tpl_vars['display'] == 'detailed'): ?> <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeTime']; ?>
 <?php endif; ?>
  </TD>
  <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['standing']; ?>
</TD>
  <TD align="center" NOWRAP><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
</TD>
  <TD>&nbsp;</TD>
<?php if ($this->_tpl_vars['userType'] != 'client'): ?>
  <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['vendor']; ?>
</TD>
  <TD>
    <A onclick="return confirm('Are you sure?');" href="deleteTxt.php?goTo=<?php echo $this->_tpl_vars['goTo']; ?>
&tradeId=<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
">
    <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['standing'] != 'Close'): ?>
      Delete
    <?php else: ?>
      Delete Stand
    <?php endif; ?>
    </A>
    &nbsp;
    <A href="<?php echo $this->_tpl_vars['edit1File']; ?>
?tradeId=<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
">Edit1</A>
    &nbsp;
    <A href="<?php echo $this->_tpl_vars['edit2File']; ?>
?tradeId=<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
">Edit2</A>
  </TD>
<?php endif; ?>
  <?php if ($this->_tpl_vars['display'] == 'detailed'): ?> 
    <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['userRemarks']; ?>
</TD>
    <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['ownClient']; ?>
</TD>
    <TD align="center" NOWRAP><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeRefNo']; ?>
</TD>
  <?php endif; ?>
</TR>
<?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['dispGross'] != 0): ?>
  <TR>
    <TD align="right" NOWRAP>
      Net: <?php echo smarty_function_math(array('equation' => "totBuyQty-totSellQty",'totBuyQty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty'],'totSellQty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty']), $this);?>

    </TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyRash']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellRash']; ?>
</TD>
  <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty'] == $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty']): ?> 
    <TD colspan="3" align="right" NOWRAP>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['profitLoss'] < 0): ?>
        <FONT class="lossStyle">Loss : 
      <?php else: ?>
        <FONT class="profitStyle">Profit : 
      <?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['profitLoss']; ?>
</FONT>
      Brok       : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['oneSideBrok']; ?>
</TD>
    <TD align="right" NOWRAP>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['netProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['netProfitLoss']; ?>
</FONT>
    </TD>
    <TD colspan="2">&nbsp;</TD>
  <?php else: ?>
    <TD colspan="6"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
 : Buy Sell Qty Not Same</TD>
  <?php endif; ?>
  </TR>
  <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['dispClientWhole'] != 0): ?>
  <TR>
    <TD colspan="5" align="right">
      : Total : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
 : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientName']; ?>

    </TD>
    <TD colspan="3" align="right"><U>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotProfitLoss'] < 0): ?>
        <FONT class="lossStyle">Loss : 
      <?php else: ?>
        <FONT class="profitStyle">Profit : 
      <?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotProfitLoss']; ?>
</FONT></U>
      Brok       : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotBrok']; ?>
</U>
    </TD>
    <TD align="right"><U>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotNetProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotNetProfitLoss']; ?>
</FONT></U></TD>
    <TD align="center" colspan="2">
      &nbsp;</TD>
  </TR>
  <?php endif; ?>
<?php endif; ?>
<?php endfor; endif; ?>
<TR><TD colspan="11">&nbsp;</TD></TR>
<TR>
  <TD align="center">Net</TD>
  <TD>Buy</TD>
  <TD>Rash</TD>
  <TD>Sell</TD>
  <TD>Rash</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD align="center">NetProfitLoss</TD>
</TR>
<?php unset($this->_sections['sec2']);
$this->_sections['sec2']['name'] = 'sec2';
$this->_sections['sec2']['loop'] = is_array($_loop=($this->_tpl_vars['wholeItemArr'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec2']['show'] = true;
$this->_sections['sec2']['max'] = $this->_sections['sec2']['loop'];
$this->_sections['sec2']['step'] = 1;
$this->_sections['sec2']['start'] = $this->_sections['sec2']['step'] > 0 ? 0 : $this->_sections['sec2']['loop']-1;
if ($this->_sections['sec2']['show']) {
    $this->_sections['sec2']['total'] = $this->_sections['sec2']['loop'];
    if ($this->_sections['sec2']['total'] == 0)
        $this->_sections['sec2']['show'] = false;
} else
    $this->_sections['sec2']['total'] = 0;
if ($this->_sections['sec2']['show']):

            for ($this->_sections['sec2']['index'] = $this->_sections['sec2']['start'], $this->_sections['sec2']['iteration'] = 1;
                 $this->_sections['sec2']['iteration'] <= $this->_sections['sec2']['total'];
                 $this->_sections['sec2']['index'] += $this->_sections['sec2']['step'], $this->_sections['sec2']['iteration']++):
$this->_sections['sec2']['rownum'] = $this->_sections['sec2']['iteration'];
$this->_sections['sec2']['index_prev'] = $this->_sections['sec2']['index'] - $this->_sections['sec2']['step'];
$this->_sections['sec2']['index_next'] = $this->_sections['sec2']['index'] + $this->_sections['sec2']['step'];
$this->_sections['sec2']['first']      = ($this->_sections['sec2']['iteration'] == 1);
$this->_sections['sec2']['last']       = ($this->_sections['sec2']['iteration'] == $this->_sections['sec2']['total']);
?>
<TR>
    <TD align="right" NOWRAP>
      <?php echo smarty_function_math(array('equation' => "buyQty-sellQty",'buyQty' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyQty'],'sellQty' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellQty']), $this);?>

    </TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyQty']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyRash']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellQty']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellRash']; ?>
</TD>
  <TD align="right" colspan="2" NOWRAP><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['itemIdExpiry']; ?>
</TD>
  <TD align="right" NOWRAP>
    <?php if ($this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['profitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
    <?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['profitLoss']; ?>
</FONT></TD>
  <TD align="right" colspan="2"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['oneSideBrok']; ?>
</TD>
  <TD align="right" NOWRAP>
    <?php if ($this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['netProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
    <?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['netProfitLoss']; ?>
</FONT></TD>
</TR>
<?php endfor; endif; ?>
<TR>
    <TD align="right" NOWRAP>
      <?php echo smarty_function_math(array('equation' => "buyQty-sellQty",'buyQty' => $this->_tpl_vars['wholeBuyQty'],'sellQty' => $this->_tpl_vars['wholeSellQty']), $this);?>

    </TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeBuyQty']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeBuyRash']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeSellQty']; ?>
</TD>
  <TD align="right"><?php echo $this->_tpl_vars['wholeSellRash']; ?>
</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" NOWRAP>
    <?php if ($this->_tpl_vars['wholeProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
    <?php echo $this->_tpl_vars['wholeProfitLoss']; ?>
</FONT></TD>
  <TD align="right" colspan="2"><?php echo $this->_tpl_vars['wholeOneSideBrok']; ?>
</TD>
  <TD align="right" NOWRAP>
    <?php if ($this->_tpl_vars['wholeNetProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
    <?php echo $this->_tpl_vars['wholeNetProfitLoss']; ?>
</FONT></TD>
</TR>
</TABLE>
</BODY>
</HTML>