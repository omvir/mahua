<?php
session_start();
if(!isset($_SESSION['toDate'])) 
  header("Location:selectDtSession.php?goTo=standing");
else
{
  //include om_config.inc
  include "etc/om_config.inc";
    //declare smarty class to object
  $smarty  = new SmartyWWW();
  // if post :Start
  if(isset($_POST['expiryId']))
  {
  	$closeDate = $_POST['standingDtCurrentYear']."-".$_POST['standingDtCurrentMonth']."-".$_POST['standingDtCurrentDay'];
  	$openDate  = $_POST['standingDtNextYear']."-".$_POST['standingDtNextMonth']."-".$_POST['standingDtNextDay'];
    while(list($key,$val) = each ($_POST['expiryId']) )
    {
      //if value greate than 0 than insert
      if($_POST['expiryId'][$key] > 0)
      {
      	$selectQuery = "SELECT * FROM expiry WHERE expiryId = '".$key."'";
      	$selectQueryResult = mysql_query($selectQuery);
      	while($expiryRow = mysql_fetch_array($selectQueryResult))
      	{
      		$itemIdPost = $expiryRow['itemId'];
      		$expiryDatePost = $expiryRow['expiryDate'];
      		$exchangePost = $expiryRow['exchange'];
      	}
        // inserting Record :Start
        $itemIdExpiryDate = $itemIdPost."".$expiryDatePost;
        $insertQuery = "INSERT INTO standing (standingDtCurrent,standingDtNext,itemIdExpiryDate,
                                              standingPrice,exchange)
                                      VALUES ('".$closeDate."','".$openDate."','".$itemIdExpiryDate."',
                                              '".$_POST['expiryId'][$key]."','".$exchangePost."')";
        $insertQueryResult = mysql_query($insertQuery);
        $id = mysql_insert_id();
        if(!$insertQueryResult)
          echo "Error".mysql_error();
        // inserting Record :End
      }
    }
    header("Location: ./standingNames.php?standingId=".$id."&standingDtCurrent=".$closeDate);
  }
  // if post :End
  // select Query :Start
  $selectQuery = "SELECT * FROM expiry
                  ORDER BY exchange, itemId, expiryDate";
  $selectQueryResult = mysql_query($selectQuery);
  $i = 0;
  $standingArray = array();
  while($row = mysql_fetch_array($selectQueryResult))
  {
    //create 2 dimentional array
  	$standingArray[$i]['expiryId']   = $row['expiryId'];
  	$standingArray[$i]['exchange']   = $row['exchange'];
  	$standingArray[$i]['itemId']     = $row['itemId'];
  	$standingArray[$i]['expiryDate'] = $row['expiryDate'];
  	$i++;
  }
  // select Query :End

  $standingDtNextSelected = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
  $smarty->assign("standingDtNextSelected",$standingDtNextSelected);
  $smarty->assign("standingArray",$standingArray);
  $smarty->assign("PHP_SELF",$_SERVER['PHP_SELF']);
  $smarty->display("standing.tpl");
}
?>