<?php
  include "./etc/om_config.inc";
session_start();
if(!isset($_SESSION['user'])
   || (isset($_SESSION['userType']) && isset($filesArr['mnuAccount'][$_SESSION['userType']]) 
        && $filesArr['mnuAccount'][$_SESSION['userType']] == 0))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:index.php");
}
else
{  
  $smarty = new SmartyWWW();
  $smarty->display("mnuAccount.tpl");
}
?>