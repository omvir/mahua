<?php
  //1019TRD.TXT
  
session_start();
if(!isset($_SESSION['toDate'])) 
{
  header("Location: selectDtSession.php?goTo=takeODINTrades");
}
else
{
  include "./etc/om_config.inc";

  $selectQuery = "SELECT * FROM `settings` 
                  WHERE `key` LIKE CONVERT( _utf8 'odinTxtFilePath' USING latin1 ) 
                               COLLATE latin1_swedish_ci
                 ";
  $selectResult = mysql_query($selectQuery) OR die(mysql_error().$selectQuery);
  if($selectRow = mysql_fetch_array($selectResult))
  {
    $filePath = $selectRow['value'];
  }
  
  $toDateFileName = substr($_SESSION['toDate'],5,2).
                    substr($_SESSION['toDate'],8,2).
                    "TRD.TXT";
  
  $row = 0;
  $handle = fopen($filePath."/".$toDateFileName, "r");
  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
  {
    $num = count($data);
    $row++;
    $expiryDate = trim($data[5]);
    $tradeRefNo = trim($data[0])." # ".trim($data[25]);

    if ($data[14]==1)
      $buySell = "Buy";
    else
      $buySell = "Sell";

    $monthStr = substr($data[23],3,3);
    switch ($monthStr)
    {
      case "JAN":
        $monthInt = '01';
        break;
      case "FEB":
        $monthInt = '02';
        break;
      case "MAR":
        $monthInt = '03';
        break;
      case "APR":
        $monthInt = '04';
        break;
      case "MAY":
        $monthInt = '05';
        break;
      case "JUN":
        $monthInt = '06';
        break;
      case "JUL":
        $monthInt = '07';
        break;
      case "AUG":
        $monthInt = '08';
        break;
      case "SEP":
        $monthInt = '09';
        break;        
      case "OCT":     
        $monthInt = '10';
        break;
      case "NOV":
        $monthInt = '11';
        break;
      case "DEC":
        $monthInt = '12';
        break;
    }
    
    $tradeDate = substr($data[23],7,4);
    $tradeDate .= '-';
    $tradeDate .= $monthInt;
    $tradeDate .= '-';
    //$date .= substr($data[23],3,3); 
    $tradeDate .= substr($data[23],0,2);
    $tradeTime = substr($data[23],12,8);

    //Checking of tradeRefNo in table start
    $selectTradeRefNoQuery = "SELECT * FROM tradetxt
                                WHERE tradeRefNo = '".$tradeRefNo."'";
    $selectTradeRefNoResult = mysql_query($selectTradeRefNoQuery);
  
    $storeThisRow = true;
    while($selectTradeRefNoRow = mysql_fetch_array($selectTradeRefNoResult))
    {
      $storeThisRow = false;
    }
    //Checking of tradeRefNo in table over

    if($storeThisRow)
    {
      $qty = $data[15];
      $itemID = trim($data[4]);
      $brok = "0";
      
      $firstName = '';
      $middleName = '';
      $lastName = '';
      //////////
      $clientQuery = "SELECT * FROM client WHERE clientId = '".trim($data[28])."'";
      $clientResult = mysql_query($clientQuery);
      if($clientRow = mysql_fetch_array($clientResult))
      {
        $firstName  = $clientRow['firstName'];
        $middleName = $clientRow['middleName'];
        $lastName   = $clientRow['lastName'];
      }
      //////////
      /////////////////
      $actualQty = $qty;
      $itemQuery = "SELECT * FROM item
                    WHERE itemId LIKE '".trim($data[4])."'";
      $itemResult = mysql_query($itemQuery);
      if($itemRow = mysql_fetch_array($itemResult))
      {
        $actualQty = $qty * $itemRow['min'];
      }
      /////////////////
      $insertQuery = "INSERT INTO tradetxt (buySell,itemId,qty,price,expiryDate,tradeRefNo,tradeDate,tradeTime,brok,vendor,clientId,firstName,middleName,lastName)
                        VALUES ('$buySell','$itemID','".$actualQty."','$data[16]','$data[5]','".$tradeRefNo."','".$tradeDate."','".$tradeTime."','$brok','".trim($data[19])."','".trim($data[28])."','".$firstName."','".$middleName."','".$lastName."')";
      $result = mysql_query($insertQuery)or die("Insert Query Failed".mysql_error());
    }
  }
  fclose($handle);
  
  header("Location: ./index.php");
}  
?>