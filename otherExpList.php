<?php
include "./etc/om_config.inc";
session_start();
if(!isset($_SESSION['user'])
   || (isset($_SESSION['userType']) && isset($filesArr['otherExpList'][$_SESSION['userType']]) 
        && $filesArr['otherExpList'][$_SESSION['userType']] == 0))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:index.php");
}
else
{
  include "./etc/functions.inc";
  $smarty=new SmartyWWW();
  $selectExp = "SELECT * FROM otherexp";
  $resultExp = mysql_query($selectExp);
  $i=0;
  $totalExp =0;
  while($rowExp = mysql_fetch_array($resultExp))
  {
  	$otherExpName[$i] = $rowExp['otherExpName'];
  	$otherExpDate[$i] = mysqlToDDMMYY($rowExp['otherExpDate']);
  	$otherExpAmount[$i] = $rowExp['otherExpAmount'];
  	$totalExp += $rowExp['otherExpAmount'];
  	$otherExpMode[$i] = $rowExp['otherExpMode'];
  	$otherExpNote[$i] = $rowExp['note'];
  	$i++;
  }
  $smarty->assign("otherExpMode",$otherExpMode);
  $smarty->assign("otherExpName",$otherExpName);
  $smarty->assign("otherExpDate",$otherExpDate);
  $smarty->assign("otherExpAmount",$otherExpAmount);
  $smarty->assign("otherExpNote",$otherExpNote);
  $smarty->assign("totalExp",$totalExp);
  $smarty->assign("i",$i);
  
  $smarty->display("otherExpList.tpl");
}
?>